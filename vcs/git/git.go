package git

import (
	"context"
	"fmt"
	"os/exec"
	"strings"
)

// VCS wraps `git`
type VCS struct {
	Root string
}

// CloneToDir clones the repo into a directory and checks out the commit.
func (v *VCS) CloneToDir(ctx context.Context, dst, commit string) error {

	revCommit, revErr := v.ResolveToCommit(ctx, commit)
	if revErr != nil {
		return revErr
	}

	cloneCmd := exec.CommandContext(ctx, "git", "clone", "-s", "-l", "-n", v.Root, dst)
	if output, cloneErr := cloneCmd.CombinedOutput(); cloneErr != nil {
		return fmt.Errorf("failed to clone repository from %q to %q; %s: %s",
			v.Root, dst, cloneErr, string(output))
	}
	strippedCommit := strings.TrimSpace(revCommit)

	coCmd := exec.CommandContext(ctx, "git", "-C", dst, "checkout", strippedCommit)
	if coOutput, coErr := coCmd.CombinedOutput(); coErr != nil {
		return fmt.Errorf("failed to checkout revision %q in %q; %s: %s",
			strippedCommit, dst, coErr, string(coOutput))
	}
	return nil
}

// ResolveToCommit resolves the commitish (a branch-name, tag, etc.) to a commit-hash
func (v *VCS) ResolveToCommit(ctx context.Context, commitish string) (string, error) {
	revparseCmd := exec.CommandContext(ctx, "git", "-C", v.Root, "rev-parse", "--verify", commitish+"^{commit}")
	revCommit, revErr := revparseCmd.CombinedOutput()
	if revErr != nil {
		return "", fmt.Errorf("failed to resolve commitish %q to hash: %s (%s)", commitish, string(revCommit), revErr)
	}
	return string(revCommit), nil
}
