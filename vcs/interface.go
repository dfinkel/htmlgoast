package vcs

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/tools/go/vcs"
	"golang.spin-2.net/htmlgoast/vcs/git"
)

// Interface implementations wrap version control systems
type Interface interface {
	ResolveToCommit(ctx context.Context, commitish string) (string, error)
	CloneToDir(ctx context.Context, dst, commit string) error
}

// Find uses golang.org/x/tools/go/vcs to find an appropriate filename
func Find(dir string) (Interface, error) {
	parts := strings.SplitN(dir, string(os.PathSeparator), 3)
	if len(parts) < 2 {
		return nil, fmt.Errorf(
			"VCS handling requires the repository root be at least one level deep, current directory (%q) is %d deep",
			dir, len(parts))
	}
	baseroot := "/" + parts[1]
	cmd, root, err := vcs.FromDir(dir, baseroot)
	if err != nil {
		return nil, fmt.Errorf("failed to find repo: %s", err)
	}
	switch cmd.Cmd {
	case "git":
		return &git.VCS{Root: filepath.Join(baseroot, root)}, nil
	}
	return nil, fmt.Errorf("unhandled cmd type: %+v", cmd)
}
