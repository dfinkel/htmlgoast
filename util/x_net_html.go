package util

import (
	"fmt"
	"io"
	"strings"
	"unicode/utf8"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

// NewEmptyHTMLDocument creates a new HTMLDocument representing an empty
// document. (it will create a document Node of type Document with no children)
func NewEmptyHTMLDocument() (*HTMLDocument, *HTMLNode) {
	docNode := &html.Node{
		Type: html.DocumentNode,
	}
	docHTMLNode := &HTMLNode{elem: docNode}
	doc := &HTMLDocument{
		root: docNode,
		m:    map[*html.Node]*HTMLNode{docNode: docHTMLNode},
	}
	docHTMLNode.doc = doc
	return doc, docHTMLNode
}

// ParseHTML invokes the x/net/html parser on its input
func ParseHTML(r io.Reader) (*HTMLDocument, *HTMLNode, error) {
	n, err := html.Parse(r)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to parse input: %s", err)
	}
	doc := &HTMLDocument{
		root: n,
		m:    map[*html.Node]*HTMLNode{},
	}
	return doc, doc.newHTMLNodeForNode(n), nil

}

func (h *HTMLDocument) newHTMLNodeForNode(n *html.Node) *HTMLNode {
	if inner, ok := h.m[n]; ok {
		// it already exists, just return that pointer
		return inner
	}

	inner := &HTMLNode{elem: n, doc: h}
	h.m[n] = inner
	// create entries for any children that don't exist in the map yet
	for child := n.FirstChild; child != nil; child = child.NextSibling {
		h.newHTMLNodeForNode(child)
	}

	return inner
}

// HTMLDocument wraps the root node of a golang.org/x/net.html/Node tree,
// implementing DomDocument
type HTMLDocument struct {
	root *html.Node
	m    map[*html.Node]*HTMLNode
}

// CreateElement creates a new HTMLNode of element type
func (h *HTMLDocument) CreateElement(Tag HTMLElemType) DomNode {
	a := atom.Lookup([]byte(strings.ToLower(string(Tag))))
	n := &HTMLNode{
		elem: &html.Node{
			Parent:      nil,
			FirstChild:  nil,
			LastChild:   nil,
			PrevSibling: nil,
			NextSibling: nil,
			Type:        html.ElementNode,
			DataAtom:    a,
			Data:        string(Tag),
			Namespace:   "",
			Attr:        []html.Attribute{},
		},
		doc: h,
	}
	h.m[n.elem] = n
	return n
}

// CreateTextNode creates a new HTMLNode of text type
func (h *HTMLDocument) CreateTextNode(contents string) DomNode {
	n := &HTMLNode{
		elem: &html.Node{
			Parent:      nil,
			FirstChild:  nil,
			LastChild:   nil,
			PrevSibling: nil,
			NextSibling: nil,
			Type:        html.TextNode,
			DataAtom:    0,
			Data:        contents,
			Namespace:   "",
			Attr:        nil,
		},
		doc: h,
	}
	h.m[n.elem] = n
	return n
}

// FindByID traverses the tree, looking for the first node with that ID as an
// attribute
func (h *HTMLDocument) FindByID(id string) DomNode {
	const IDAttrName = "id"
	hasID := func(n *html.Node) bool {
		for _, attr := range n.Attr {
			if !strings.EqualFold(attr.Key, IDAttrName) {
				continue
			}
			if attr.Val == id {
				return true
			}
		}
		return false
	}

	var findID func(n *html.Node) *html.Node

	findID = func(n *html.Node) *html.Node {
		if hasID(n) {
			return n
		}
		if n.FirstChild != nil {
			if c := findID(n.FirstChild); c != nil {
				return c
			}
		}
		if n.NextSibling != nil {
			if c := findID(n.NextSibling); c != nil {
				return c
			}
		}
		return nil
	}

	n := findID(h.root)
	if n == nil {
		return nil
	}
	return h.m[n]
}

// Render uses golang.org/x/net/html to render the document to the passed
// writer.
func (h *HTMLDocument) Render(w io.Writer) error {
	return html.Render(w, h.root)
}

// HTMLNode wraps a golang.org/x/net/html.Node implementing DomNode
type HTMLNode struct {
	elem *html.Node
	doc  *HTMLDocument
}

// TagName returns the HTML tag
func (h *HTMLNode) TagName() string {
	if h.elem.DataAtom == 0 {
		return h.elem.Data
	}
	return h.elem.DataAtom.String()
}

// SetText removes all children, replacing them with a single text node.
func (h *HTMLNode) SetText(newText string) {
	if h.elem.Type == html.TextNode {
		h.elem.Data = newText
		return
	}
	if h.elem.FirstChild != nil {
		for child := h.elem.FirstChild; child != nil; child = child.NextSibling {
			h.elem.RemoveChild(child)
		}
	}
	// at this point h.elem has no children.
	newChild := h.doc.CreateTextNode(newText).(*HTMLNode).elem
	h.elem.AppendChild(newChild)
}

func htmlChildText(n *html.Node) string {
	if n.Type == html.TextNode {
		return n.Data
	}
	if n.Type == html.ElementNode {
		switch n.DataAtom {
		case atom.Br, atom.P:
			return "\n"
		}
	}

	b := strings.Builder{}
	// depth-first traversal as per the DOM-spec
	if n.FirstChild != nil {
		for child := n.FirstChild; child != nil; child = child.NextSibling {
			b.WriteString(htmlChildText(child))
		}
	}
	return b.String()
}

// ChildText returns (an approximation of) the text in the child nodes of this
// element
func (h *HTMLNode) ChildText() string {
	return htmlChildText(h.elem)
}

// ChildLength returns the number of runes in the children of this node
func (h *HTMLNode) ChildLength() int {
	return utf8.RuneCountInString(htmlChildText(h.elem))
}

// Parent returns the parent node (if any)
func (h *HTMLNode) Parent() DomNode {
	return h.doc.m[h.elem.Parent]
}

// AppendChild appends a new child node
func (h *HTMLNode) AppendChild(newChild DomNode) {
	h.elem.AppendChild(newChild.(*HTMLNode).elem)
}

// ReplaceChild replaces a child node with a new one
func (h *HTMLNode) ReplaceChild(newNode DomNode, oldNode DomNode) {
	oldChild := oldNode.(*HTMLNode)
	newChild := newNode.(*HTMLNode)
	h.elem.InsertBefore(newChild.elem, oldChild.elem)
	h.elem.RemoveChild(oldChild.elem)
}

// Returns the node if found, the relative offset of the found node and
// (independent of successfully finding the requested offset in a node) the
// length of this node, and its children.
func findHTMLTextNodeWithOffset(n *html.Node, offset int) (*html.Node, int, int) {
	if n.Type == html.TextNode {
		dataLen := utf8.RuneCountInString(n.Data)
		if dataLen > offset {
			return n, 0, dataLen
		}
		return nil, 0, dataLen
	}
	// Br tags can't have children
	if n.DataAtom == atom.Br {
		if offset == 0 {
			return n, 0, 1
		}
		return nil, 0, 1
	}
	// This isn't a text node, so check for children.
	consumed := 0

	for child := n.FirstChild; child != nil; child = child.NextSibling {
		tn, foundOffset, l := findHTMLTextNodeWithOffset(child, offset-consumed)
		if tn != nil {
			return tn, foundOffset + consumed, l
		}
		consumed += l
	}

	return nil, 0, consumed
}

func translateUTF8Offset(s string, offset int) (int, error) {
	if len(s) < offset {
		return -1, &OutOfBoundsErr{
			Length:          len(s),
			Offset:          0,
			RequestedLength: offset,
		}
	}
	if offset == 0 {
		return 0, nil
	}
	byteOffset := 0
	runeOffset := 0
	reader := strings.NewReader(s)
	for _, sz, err := reader.ReadRune(); err == nil && runeOffset < offset; _, sz, err = reader.ReadRune() {
		byteOffset += sz
		runeOffset++
	}

	if runeOffset != offset {
		return -1, fmt.Errorf("unable to resolve byte offset of rune; byte offset %d, rune offset %d; requested rune offset %d",
			byteOffset, runeOffset, offset)
	}

	return byteOffset, nil
}

// Split splits out a substring of a descendant text node into its own node,
// returning the new node. [start, end) must form a half-open set, where the
// (0-indexed) start is inclusive and end is exclusive.
func (h *HTMLNode) Split(start int, end int) (DomNode, error) {
	splittingNode, foundOffset, nlen := findHTMLTextNodeWithOffset(h.elem, start)

	if splittingNode == nil {
		return nil, fmt.Errorf("failed to find appropriate node for range [%d, %d)",
			start, end)
	}
	innerOffset, startOffsetErr := translateUTF8Offset(splittingNode.Data, start-foundOffset)
	if startOffsetErr != nil {
		return nil, fmt.Errorf("unexpected start offset: %d; node length %d (contents %q); at offset %d: %s",
			start-foundOffset, nlen, splittingNode.Data, foundOffset, startOffsetErr)
	}
	innerEndOffset, endOffsetErr := translateUTF8Offset(splittingNode.Data, end-foundOffset)
	if innerEndOffset < 0 {
		return nil, fmt.Errorf("unexpected end offset: %d; node length %d (contents %q); at offset %d: %s",
			end-foundOffset, nlen, splittingNode.Data, foundOffset, endOffsetErr)
	}

	innerLen := end - start
	if (innerLen + innerOffset) > nlen {
		return nil, &OutOfBoundsErr{
			Length:          nlen,
			RequestedLength: innerLen,
			Offset:          innerOffset,
		}
	}
	// if the existing node already matches our boundaries, return it without
	// further ado.
	if innerLen == nlen && innerOffset == 0 {
		return h.doc.m[splittingNode], nil
	}

	var successorNode *html.Node
	origText := splittingNode.Data
	if start != foundOffset {
		// if there is text preceding our desired offset, leave it in the
		// original node (otherwise, we're going to leave the original node at
		// the end).
		splittingNode.Data = origText[:innerOffset]
		if end == foundOffset+nlen {
			// the end of the requested range matches, so our new node will be
			// the immediate successor to splittingNode
			NewNode := h.doc.CreateTextNode(origText[innerOffset:]).(*HTMLNode)
			if splittingNode.Parent != nil {
				if splittingNode.NextSibling != nil {
					splittingNode.Parent.InsertBefore(NewNode.elem, splittingNode.NextSibling)
				} else {
					splittingNode.Parent.AppendChild(NewNode.elem)
				}
			}
			return NewNode, nil
		}
		// if we aren't consuming to the end of the text node we need to create
		// another node for the remainder.
		suffixNode := h.doc.CreateTextNode(origText[innerEndOffset:]).(*HTMLNode)
		if splittingNode.Parent != nil {
			if splittingNode.NextSibling != nil {
				splittingNode.Parent.InsertBefore(suffixNode.elem, splittingNode.NextSibling)
			} else {
				splittingNode.Parent.AppendChild(suffixNode.elem)
			}
		}
		successorNode = suffixNode.elem
	} else {
		successorNode = splittingNode
		splittingNode.Data = origText[innerEndOffset:]
	}
	NewNode := h.doc.CreateTextNode(origText[innerOffset:innerEndOffset]).(*HTMLNode)
	if splittingNode.Parent != nil {
		splittingNode.Parent.InsertBefore(NewNode.elem, successorNode)
	}

	return NewNode, nil
}

// SetAttr sets an attribute on an HTML node
func (h *HTMLNode) SetAttr(name string, val string) {
	for i, attr := range h.elem.Attr {
		if strings.EqualFold(attr.Key, name) {
			h.elem.Attr[i].Val = val
			return
		}
	}
	h.elem.Attr = append(h.elem.Attr, html.Attribute{
		Key: name,
		Val: val,
	})
}

// GetAttr returns the first value of an attribute with the requested name.
func (h *HTMLNode) GetAttr(name string) string {
	for _, attr := range h.elem.Attr {
		if attr.Key == name {
			return attr.Val
		}
	}
	return ""
}

// SetClass sets the class attribute of a node
func (h *HTMLNode) SetClass(name string) {
	h.SetAttr("class", name)
}
