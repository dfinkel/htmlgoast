package util

import (
	"fmt"
	"net/url"
)

// NewAnchor creates a new A (anchor) element
func NewAnchor(doc DomDocument, href *url.URL, childNode DomNode) DomNode {
	anch := doc.CreateElement(Anchor)
	anch.SetAttr("href", href.String())
	anch.AppendChild(childNode)
	return anch
}

// NewDIV creates a new DIV element ready for insertion into the DOM.
func NewDIV(doc DomDocument, id string, initText string) DomNode {
	newDiv := doc.CreateElement("DIV")

	// Initialize our new div with an unformated innerText that matches the
	// current contents of the exiting div.
	newDiv.SetText(initText)

	newDiv.SetAttr("contenteditable", "")
	newDiv.SetAttr("id", id)
	newDiv.SetAttr("spellcheck", "false")

	return newDiv
}

// SetRangeClass splits out an appropriate text node and creates a new span
// node with the appropriate class, then reparents the newly split-out
// text-node under it.
func SetRangeClass(elemType HTMLElemType, doc DomDocument, outer DomNode, class string, start, end int) (DomNode, error) {
	txtNode, splitErr := outer.Split(start, end)
	if splitErr != nil {
		return nil, fmt.Errorf("failed to split out node contents on [%d, %d): %s",
			start, end, splitErr)
	}
	newSpan := doc.CreateElement(elemType)
	newSpan.SetClass(class)
	txtNode.Parent().ReplaceChild(newSpan, txtNode)
	newSpan.AppendChild(txtNode)
	return newSpan, nil
}
