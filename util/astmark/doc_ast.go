package astmark

import (
	"go/ast"
	"go/token"
	"strings"

	"golang.spin-2.net/htmlgoast/util"
)

// Doc tracks a util.Document, a Dom tree and a token.fileset to easilly mark
// AST nodes based on passed tokens.
type Doc struct {
	// top-level DOM document object
	doc util.DomDocument

	// outermost node containing go source
	container util.DomNode

	// fileset into which the file was parsed
	fs *token.FileSet

	// source contains the original source code so we can translate between
	// rune and byte offsets
	source string
}

// NewDoc constructs a new Doc from its arguments
func NewDoc(doc util.DomDocument, div util.DomNode, fs *token.FileSet, src string) *Doc {
	return &Doc{
		doc:       doc,
		container: div,
		fs:        fs,
		source:    src,
	}
}

// SetTokenClass splits out the range associated with a token and sets its CSS
// class attribute to the class argument, returning the new node and an error.
func (d *Doc) SetTokenClass(elemType util.HTMLElemType, tok token.Token, pos token.Pos, class string) (util.DomNode, error) {
	if pos == token.NoPos {
		return nil, nil
	}
	return util.SetRangeClass(elemType, d.doc, d.container, class,
		d.offsetOf(pos), d.offsetOf(pos)+len(tok.String()))
}

// SetTokenClassNoNode splits out the range associated with a token and sets
// its CSS class attribute to the class argument and does not return the new
// node.
func (d *Doc) SetTokenClassNoNode(elemType util.HTMLElemType, tok token.Token, pos token.Pos, class string) error {
	_, err := d.SetTokenClass(elemType, tok, pos, class)
	return err
}

// SetRangeClass splits out the range associated with an ast.Node and sets its CSS
// class attribute to the class argument, returning the new node and an error.
func (d *Doc) SetRangeClass(elemType util.HTMLElemType, class string, n ast.Node) (util.DomNode, error) {
	start, end := d.offsetRangeOf(n)
	return util.SetRangeClass(elemType, d.doc, d.container, class, start, end)
}

// SetRangeClassNoNode splits out the range associated with an ast.Node and sets its CSS
// class attribute to the class argument, returning the new node and an error.
func (d *Doc) SetRangeClassNoNode(elemType util.HTMLElemType, class string, n ast.Node) error {
	_, err := d.SetRangeClass(elemType, class, n)
	return err
}

func byteOffsetToRuneOffset(s string, offset int) int {
	runeOffset := 0
	byteOffset := 0
	reader := strings.NewReader(s)
	for _, sz, err := reader.ReadRune(); err == nil && byteOffset < offset; _, sz, err = reader.ReadRune() {
		byteOffset += sz
		runeOffset++
	}

	return runeOffset
}

func (d *Doc) offsetOf(pos token.Pos) int {
	return byteOffsetToRuneOffset(d.source, d.fs.Position(pos).Offset)
}
func (d *Doc) offsetRangeOf(n ast.Node) (int, int) {
	return d.offsetOf(n.Pos()), d.offsetOf(n.End())
}
