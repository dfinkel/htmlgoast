// +build wasm,js

package util

import (
	"fmt"
	"log"
	"syscall/js"
	"unicode/utf8"
)

// JSDomNode objects wrap nodes
type JSDomNode struct {
	jsVal js.Value
}

// TagName implements DomNode
func (j *JSDomNode) TagName() string {
	return j.jsVal.Get("tagName").String()
}

// ChildText implements DomNode
func (j *JSDomNode) ChildText() string {
	if nodeDomType(j.jsVal.Get("nodeType").Int()) == textNodeDOMType {
		return j.jsVal.Get("data").String()
	}
	return j.jsVal.Get("innerText").String()
}

// ChildLength implements DomNode
func (j *JSDomNode) ChildLength() int {
	return utf8.RuneCountInString(j.ChildText())
}

// AppendChild adds its argument as a child of the receiver node
func (j *JSDomNode) AppendChild(child DomNode) {
	jsChild := child.(*JSDomNode)

	j.jsVal.Call("appendChild", jsChild.jsVal)
}

// Parent implements DomNode
func (j *JSDomNode) Parent() DomNode {
	parent := j.jsVal.Get("parentNode")
	if parent.Type() == js.TypeUndefined {
		log.Printf("Parent() called on node with no parent: %v; contents: %s",
			j.jsVal.String(), j.ChildText())
		return nil
	}
	return &JSDomNode{jsVal: parent}
}

// ReplaceChild implements DomNode
func (j *JSDomNode) ReplaceChild(newNode DomNode, oldNode DomNode) {
	// type-assert back to the same type we're using here.
	jsNewNode := newNode.(*JSDomNode)
	jsOldNode := oldNode.(*JSDomNode)

	j.jsVal.Call("replaceChild", jsNewNode.jsVal, jsOldNode.jsVal)
}

// Split implements DomNode
func (j *JSDomNode) Split(start int, end int) (DomNode, error) {
	n, err := j.splitTextNode(j.jsVal, start, end)
	if err != nil {
		return nil, err
	}
	return &JSDomNode{jsVal: n}, nil
}

type nodeDomType int8

const (
	elemNodeDOMType nodeDomType = iota + 1
	attrNodeDOMType
	textNodeDOMType
	cdataSectionDOMNodeType
	entityRefDOMNodeType
	entityDOMNodeType
	procInstrDOMNodeType
	commentDOMNodeType
	documentNodeDOMType
	documentTypeNodeDOMType
	documentFragmentNodeDOMType
	notationNodeDOMType
)

// DomChildOffset iterates over the child elements, finding which one contains
// the byte at offset b, and returning that child as well as the offset.
func (j *JSDomNode) domChildOffset(elem js.Value, b int) (js.Value, int) {
	childList := elem.Get("childNodes")
	if childList.Type() != js.TypeObject {
		return js.Null(), -1
	}
	childCount := childList.Get("length").Int()
	charsCounted := 0
	for i := 0; i < childCount; i++ {
		child := childList.Call("item", i)
		var childText string

		switch nodeDomType(child.Get("nodeType").Int()) {
		case textNodeDOMType:
			childText = child.Get("data").String()
		case elemNodeDOMType:
			switch child.Get("tagName").String() {
			case "BR":
				childText = "\n"
			default:
				childText = child.Get("innerText").String()
			}
		case documentNodeDOMType:
			childText = child.Get("innerText").String()
		default:
			return js.Null(), -1
		}

		runes := utf8.RuneCountInString(childText)
		charsCounted += runes

		if charsCounted > b {
			return child, charsCounted - runes
		}
	}
	return js.Null(), -1
}

// TextDescendentOffset applies DomChildOffset iteratively until it gets a
// TextNode.
func (j *JSDomNode) textDescendentOffset(elem js.Value, b int) (js.Value, int) {
	offset := 0
	child := elem
	for {
		var tmpOffset int
		child, tmpOffset = j.domChildOffset(child, b-offset)

		offset += tmpOffset

		// No solution found
		if tmpOffset == -1 || child.Type() == js.TypeNull {
			return js.Null(), -1
		}

		if child.Type() != js.TypeObject {
			panic("unexpected type for node: " + child.String())
		}

		if nodeDomType(child.Get("nodeType").Int()) == textNodeDOMType {
			return child, offset
		}
	}
}

// SplitTextNode splits out the contents of the node argument and returns a new
// text node with exactly the contents on the [start, end) range.
func (j *JSDomNode) splitTextNode(node js.Value, start, end int) (js.Value, error) {
	// We need to split the text into 3 parts: before, after and the
	// import itself.

	splitNode, offset := j.textDescendentOffset(node, start)

	if splitNode.Type() == js.TypeNull {
		return js.Undefined(), fmt.Errorf("received null object (unable to find node)")
	}

	// We've now guaranteed that this is a text node, so take advantage
	// of the "data" attribute and "splitText" method.
	nodeval := splitNode.Get("data")
	log.Printf("splitting node with text len %[2]d offset %[3]d and text: %[1]q",
		nodeval, utf8.RuneCountInString(nodeval.String()), offset)
	if end-offset < utf8.RuneCountInString(nodeval.String()) {
		// only split the tail off if it'll do something
		splitNode.Call("splitText", end-offset)
	}
	if start == offset {
		return splitNode, nil
	}
	replaceText := splitNode.Call("splitText", start-offset)
	log.Printf("split node replacement: %s (contents: %q)", replaceText, replaceText.Get("data"))

	return replaceText, nil
}

// SetText sets the inner text
func (j *JSDomNode) SetText(inner string) {
	j.jsVal.Set("innerText", inner)
}

// SetAttr implements DomNode
func (j *JSDomNode) SetAttr(name string, val string) {
	j.jsVal.Call("setAttribute", name, val)
}

// GetAttr implements DomNode
func (j *JSDomNode) GetAttr(name string) string {
	return j.jsVal.Call("getAttribute", name).String()
}

// SetClass implements DomNode
func (j *JSDomNode) SetClass(val string) {
	j.jsVal.Set("className", val)
}

// JSDomDocument is an implementation of DomDocument
type JSDomDocument struct {
	jsDoc js.Value
}

// NewJSDomDocument creates a DomDocument implementation wrapping the js.Value
// for the DOM document
func NewJSDomDocument() *JSDomDocument {
	doc := js.Global().Get("document")
	return &JSDomDocument{jsDoc: doc}
}

// CreateElement creates a DOM node of type Tag.
func (j *JSDomDocument) CreateElement(Tag HTMLElemType) DomNode {
	return &JSDomNode{jsVal: j.jsDoc.Call("createElement", string(Tag))}
}

// FindByID finds the DomNode with the relevant ID
func (j *JSDomDocument) FindByID(id string) DomNode {
	return &JSDomNode{jsVal: j.jsDoc.Call("getElementById", id)}
}

// CreateTextNode creates a detached text node under this document
func (j *JSDomDocument) CreateTextNode(contents string) DomNode {
	txtNode := j.jsDoc.Call("createTextNode", contents)
	return &JSDomNode{jsVal: txtNode}
}
