package util

import (
	"strconv"
	"strings"
	"testing"
)

const basicSampleHTML = `
<html><head>Hello World!</head>
<body>

<pre><code syntax="golang">
package foo

<div>import</div> (
	<div id="fimbat-imp">"fimbat&quot;</div>
)

const microfim = "abcdμ"
const nanofim = "fimbat"

</code></pre>

</body>
</html>
`

func TestHTMLDocumentParse(t *testing.T) {
	doc, root, parseErr := ParseHTML(strings.NewReader(basicSampleHTML))
	if parseErr != nil {
		t.Fatalf("failed to parse sample HTML: %s", parseErr)
	}

	if root == nil {
		t.Fatalf("unexpectedly nil root node")
	}
	if doc == nil {
		t.Fatalf("unexpectedly nil doc node")
	}

	impLitNode := doc.FindByID("fimbat-imp")
	if impLitNode == nil {
		t.Fatalf("failed to find import path node")
	}
	if ctext := impLitNode.ChildText(); ctext != `"fimbat"` {
		t.Errorf("unexpected child text for text import node: %q; expected %q", ctext,
			`"fimbat"`)
	}
	if !strings.EqualFold(impLitNode.TagName(), "div") {
		t.Errorf("unexpected tag name for import node: %q, expected %q",
			impLitNode.TagName(), "div")
	}
	if clen := impLitNode.ChildLength(); clen != 8 {
		t.Errorf("unexpected child text length for import node %d; expected 8", clen)
	}
	if impLitNodeParent := impLitNode.Parent(); impLitNodeParent == nil {
		t.Error("unexpectedly nil parent node for import literal")
	}

	const className = "foobarbaz"
	impLitNode.SetClass(className)
	if cls := impLitNode.GetAttr("class"); cls != className {
		t.Errorf("unexpected node class: %q; expected %q", cls, className)
	}
}

func TestEmptyHTMLDocument(t *testing.T) {
	doc, root := NewEmptyHTMLDocument()

	if root == nil {
		t.Fatalf("unexpectedly nil root node")
	}
	if doc == nil {
		t.Fatalf("unexpectedly nil doc node")
	}
	const inText = "foo bar baz μ fim"
	root.SetText(inText)

	if rootChildText := root.ChildText(); rootChildText != inText {
		t.Errorf("failed to set child text; got %q, expected %q", rootChildText, inText)
	}

	NewSplit, splitErr := root.Split(8, 13)
	if splitErr != nil {
		t.Errorf("failed to split node: %s", splitErr)
	}

	expectedContents := inText[8:14]
	expectedPredContents := inText[:8]
	expectedSuccessorContents := inText[14:]
	if NewSplit.ChildText() != expectedContents {
		t.Errorf("unexpected contents for split node: %q; expected %q", NewSplit.ChildText(), expectedContents)
		t.Logf("newNode text: %q", NewSplit.(*HTMLNode).elem.Data)
	}

	if rootChildText := root.ChildText(); rootChildText != inText {
		t.Errorf("failed to re-retreive child text; got %q, expected %q", rootChildText, inText)
	}
	pred := NewSplit.(*HTMLNode).elem.PrevSibling
	successor := NewSplit.(*HTMLNode).elem.NextSibling

	if pred == nil {
		t.Fatal("child has no predecessor sibling")
	}
	if successor == nil {
		t.Fatal("child has no successor sibling")
	}
	if pred.Data != expectedPredContents {
		t.Errorf("unexpected predessor text: %q; expected: %q", pred.Data, expectedPredContents)
	}
	if successor.Data != expectedSuccessorContents {
		t.Errorf("unexpected successor text: %q; expected: %q", successor.Data, expectedSuccessorContents)
	}

	expectedContentsTwo := inText[10:12]
	newerSplit, newsplitErr := NewSplit.Split(2, 4)
	if newsplitErr != nil {
		t.Errorf("failed to split node: %s", newsplitErr)
	}

	if newerSplit.ChildText() != expectedContentsTwo {
		t.Errorf("unexpected contents for split node: %q; expected %q", newerSplit.ChildText(), expectedContentsTwo)
		t.Logf("newNode text: %q", newerSplit.(*HTMLNode).elem.Data)
	}
}

func TestEmptyHTMLDocumentDeepTree(t *testing.T) {
	doc, root := NewEmptyHTMLDocument()

	if root == nil {
		t.Fatalf("unexpectedly nil root node")
	}
	if doc == nil {
		t.Fatalf("unexpectedly nil doc node")
	}

	firstLevel := doc.CreateElement(Div)
	root.AppendChild(firstLevel)
	secondLevel := doc.CreateElement(Div)
	firstLevel.AppendChild(secondLevel)
	const inText = "foo bar baz μ fim"
	const firstText = "fim Bat Boo μ"
	textNodeOne := doc.CreateTextNode(firstText)
	secondLevel.AppendChild(textNodeOne)
	nodeOneLen := textNodeOne.ChildLength()
	textNodeTwo := doc.CreateTextNode(inText)
	secondLevel.AppendChild(textNodeTwo)

	if rootChildText := root.ChildText(); rootChildText != (firstText + inText) {
		t.Errorf("failed to set child text; got %q, expected %q", rootChildText, (firstText + inText))
	}

	NewSplit, splitErr := root.Split(nodeOneLen+1, nodeOneLen+13)
	if splitErr != nil {
		t.Errorf("failed to split node: %s", splitErr)
	}
	expectedContents := inText[1:14]
	expectedSuccessorContents := inText[14:]
	if NewSplit.ChildText() != expectedContents {
		t.Errorf("unexpected contents for split node: %q; expected %q", NewSplit.ChildText(), expectedContents)
		t.Logf("newNode text: %q", NewSplit.(*HTMLNode).elem.Data)
	}
	successor := NewSplit.(*HTMLNode).elem.NextSibling
	if successor.Data != expectedSuccessorContents {
		t.Errorf("unexpected successor text: %q; expected: %q", successor.Data, expectedSuccessorContents)
	}

}

func TestPopulateAndSlicePrefixHTMLDocument(t *testing.T) {
	for tname, tbl := range map[string]string{
		"utf8_one_mu":  "foo bar baz μ fim foo bar baz",
		"utf8_two_mus": "foo bar baz μ fimμ foo bar baz",
		"ascii":        "foo bar baz bim",
	} {
		inText := tbl
		t.Run(tname, func(t *testing.T) {

			inTextRuneSlice := []rune(inText)

			for i := 0; i <= len(inTextRuneSlice); i++ {
				i := i
				t.Run(strconv.Itoa(i), func(t *testing.T) {
					doc, root := NewEmptyHTMLDocument()

					if root == nil {
						t.Fatalf("unexpectedly nil root node")
					}
					if doc == nil {
						t.Fatalf("unexpectedly nil doc node")
					}

					root.SetText(inText)

					if rootChildText := root.ChildText(); rootChildText != inText {
						t.Errorf("failed to set child text; got %q, expected %q", rootChildText, inText)
					}

					NewSplit, splitErr := root.Split(0, i)
					if splitErr != nil {
						t.Errorf("failed to split node: %s", splitErr)
					}
					if NewSplit == nil {
						if i == 0 {
							// empty split, it's fine.
							return
						}
						t.Fatal("nil split")
					}

					expectedContents := string(inTextRuneSlice[:i])
					expectedSuccessorContents := string(inTextRuneSlice[i:])
					if NewSplit.ChildText() != expectedContents {
						t.Errorf("unexpected contents for split node: %q; expected %q", NewSplit.ChildText(), expectedContents)
						t.Logf("newNode text: %q", NewSplit.(*HTMLNode).elem.Data)
					}
					successor := NewSplit.(*HTMLNode).elem.NextSibling
					if successor == nil {
						if i != len(inTextRuneSlice) {
							t.Fatal("no successor to incomplete prefix")
						}
						return
					}
					if successor.Data != expectedSuccessorContents {
						t.Errorf("unexpected successor text: %q; expected: %q", successor.Data, expectedSuccessorContents)
					}
				})
			}
		})
	}

}
