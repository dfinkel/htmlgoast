package astlink

import (
	"net/url"
	"sort"
	"strconv"
	"strings"
)

func (d *DocFinder) pkgDocQuoted(pth string) *url.URL {
	strippedPath, err := strconv.Unquote(pth)
	if err != nil {
		return nil
	}
	return d.PkgDoc(strippedPath)
}

type privatePkgPrefix struct {
	prefix  string
	docHost string
	scheme  string
}

// DocFinder provides functionality to lookup package documentation served by
// godoc, using golang.org/pkg for standard library packages and godoc.org (or
// some replacement) for everything else.
type DocFinder struct {
	GodocHost string
	GopkgHost string
	Secure    bool

	privatePrefixes []privatePkgPrefix
}

// DefaultDocFinder returns a reasonable default value for a DocFinder
func DefaultDocFinder() DocFinder {
	return DocFinder{
		GodocHost: "godoc.org",
		GopkgHost: "golang.org",
		Secure:    true,
	}
}

// AddPrivatePrefix adds its arguments as a URL to the list of
// package-prefix-based overrides to the serving base URL.
func (d *DocFinder) AddPrivatePrefix(prefix, docHost string, https bool) {
	scheme := "https"
	if !https {
		scheme = "http"
	}
	// check whether the prefix is already present, so we can overwrite it if
	// necessary
	n := len(d.privatePrefixes)
	if n > 0 {
		offset := sort.Search(n, func(i int) bool { return d.privatePrefixes[i].prefix >= prefix })
		if offset < n && d.privatePrefixes[offset].prefix == prefix {
			d.privatePrefixes[offset].docHost = docHost
			d.privatePrefixes[offset].scheme = scheme
			return
		}
	}

	d.privatePrefixes = append(d.privatePrefixes, privatePkgPrefix{
		prefix:  prefix,
		docHost: docHost,
		scheme:  scheme,
	})
	sort.Slice(d.privatePrefixes, func(i, j int) bool {
		return d.privatePrefixes[i].prefix < d.privatePrefixes[j].prefix
	})
}

// PkgDoc provides a documentation URL for the given package based on
// configuration in d.
func (d *DocFinder) PkgDoc(pkg string) *url.URL {
	defaultScheme := "https"
	if !d.Secure {
		defaultScheme = "http"
	}
	// If non-empty, search the privatePrefixes slice for one that's a prefix
	n := len(d.privatePrefixes)
	switch n {
	case 0:
	case 1:
		if strings.HasPrefix(pkg, d.privatePrefixes[0].prefix) {
			return &url.URL{
				Scheme: d.privatePrefixes[0].scheme,
				Host:   d.privatePrefixes[0].docHost,
				Path:   pkg,
			}
		}
	default:
		offset := sort.Search(n, func(i int) bool { return d.privatePrefixes[i].prefix >= pkg })
		if offset > 0 {
			if strings.HasPrefix(pkg, d.privatePrefixes[offset-1].prefix) {
				return &url.URL{
					Scheme: d.privatePrefixes[offset-1].scheme,
					Host:   d.privatePrefixes[offset-1].docHost,
					Path:   pkg,
				}
			}

		}
		if offset < n {
			if strings.HasPrefix(pkg, d.privatePrefixes[offset].prefix) {
				return &url.URL{
					Scheme: d.privatePrefixes[offset].scheme,
					Host:   d.privatePrefixes[offset].docHost,
					Path:   pkg,
				}
			}
		}
	}
	if strings.Contains(pkg, ".") {
		return &url.URL{
			Scheme: defaultScheme,
			Host:   d.GodocHost,
			Path:   pkg,
		}
	}

	return &url.URL{
		Scheme: defaultScheme,
		Host:   d.GopkgHost,
		Path:   "/pkg/" + pkg,
	}
}
