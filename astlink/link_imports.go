package astlink

import (
	"fmt"
	"go/ast"
	"go/types"
	"path"
	"strconv"

	"golang.spin-2.net/htmlgoast/util"
)

func importFragment(imp *ast.ImportSpec) string {
	unquoted, _ := strconv.Unquote(imp.Path.Value)
	return "imp-" + unquoted
}

// SetImportMarks uses data from fs and fast to create html links to the
// relevant documentation.
func (st *State) SetImportMarks() error {

	for _, imp := range st.fast.Imports {
		if imp.Pos().IsValid() {
			importPath, unquotErr := strconv.Unquote(imp.Path.Value)
			if unquotErr != nil {
				return fmt.Errorf("failed to unquote text %q: %s", imp.Path.Value, unquotErr)
			}
			impFrag := importFragment(imp)
			name := path.Base(importPath)
			if st.ptypInfo != nil {
				typeObj := st.ptypInfo.Implicits[imp]
				if typeObj != nil {
					st.typeObjFrag[typeObj] = "#" + impFrag
					is, ok := typeObj.(*types.PkgName)
					if ok && is.Name() != "" {
						name = is.Name()
					}
				}
			}

			pathAnchor, pathSplitErr := st.marker.SetRangeClass(util.Anchor, st.classConf.Import, imp.Path)
			if pathSplitErr != nil {
				return fmt.Errorf("failed to mark import of %s as an anchor: %s", imp.Path.Value, pathSplitErr)
			}

			pathAnchor.SetAttr("name", impFrag)
			doc := st.d.pkgDocQuoted(imp.Path.Value)
			if doc != nil {
				pathAnchor.SetAttr(
					"href", doc.String())
			}
			pathAnchor.SetAttr("title", "imports "+imp.Path.Value)

			// If there's an identifier, use it.
			if imp.Name != nil {
				name = imp.Name.Name
				_, idErr := st.marker.SetRangeClass(util.Span, st.classConf.Import, imp.Name)
				if idErr != nil {
					return fmt.Errorf("failed to mark import ID %q with error: %s", imp.Name.Name, idErr)
				}
			}
			st.m[name] = imp
			st.impFrag[name] = impFragAndDoc{
				frag: "#" + impFrag,
				doc:  doc,
			}
		}
	}
	return nil
}
