package astlink

import (
	"fmt"
	"go/ast"

	"golang.spin-2.net/htmlgoast/util"
)

// MarkComments splits all comments into their own div nodes and adds class
// from classConf.Comment
func (st *State) MarkComments() error {
	for _, cgrp := range st.fast.Comments {
		for _, c := range cgrp.List {
			markErr := st.markComment(c)
			if markErr != nil {
				return fmt.Errorf("failed to mark comment %q: %s", c.Text, markErr)
			}
		}
	}
	return nil
}

func (st *State) markComment(c *ast.Comment) error {
	if _, err := st.marker.SetRangeClass(util.Span, st.classConf.Comment, c); err != nil {
		return fmt.Errorf("failed to split out comment node (%q): %s",
			c.Text, err)
	}
	return nil
}
