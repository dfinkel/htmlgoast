package astlink

import (
	"fmt"
	"go/ast"
	"go/constant"
	"go/token"
	"go/types"
	"log"
	"net/url"
	"path/filepath"
	"strconv"
	"strings"

	"golang.org/x/tools/go/ssa"

	"golang.spin-2.net/htmlgoast/util"
)

// markDeclNames is a pre-pass that just marks package-level identifiers and
// designates their fragments so a second pass can mark references to them no
// matter the order.
func (st *State) markDeclNames() error {
	errMgr := astErrorMgr{fs: st.fs}
	for _, a := range st.fast.Decls {
		switch z := a.(type) {
		case *ast.GenDecl:
			for _, decl := range z.Specs {
				switch z.Tok {
				case token.VAR, token.CONST:
					{
						class := st.classConf.Identifier
						if z.Tok == token.CONST {
							class = st.classConf.Constant
						}
						for _, n := range decl.(*ast.ValueSpec).Names {
							errMgr.accumulateError(n, st.markNewIdentWithClass(n, class))
						}
					}
				case token.TYPE:
					name := decl.(*ast.TypeSpec).Name
					errMgr.accumulateError(name, st.markNewType(name))
				}
			}
		case *ast.FuncDecl:
			errMgr.accumulateError(z, st.markNewIdentWithClass(z.Name, st.classConf.Function))
		}
	}

	if errMgr.hasErr() {
		return &errMgr
	}
	return nil
}

func (st *State) genStoreID(fname string, n *ast.Ident) {
	id := "#" + genID(n.Name, n)
	if fname != "#" {
		id = fname + id

	}
	st.objFrag[n.Obj] = id
	if st.ptypInfo != nil {
		if typeObj := st.ptypInfo.Defs[n]; typeObj != nil {
			st.typeObjFrag[typeObj] = id
		}
	}
}

// GenIDLinks populates internal map with object IDs for package-level
// identifiers in other files of the same packages so links point at those
// files rather than godoc.org.
func (st *State) GenIDLinks(files []*ast.File) {
	for _, f := range files {
		if f == st.fast {
			continue
		}
		fname := filepath.Base(st.fs.Position(f.Pos()).Filename)
		for _, a := range f.Decls {
			switch z := a.(type) {
			case *ast.GenDecl:
				for _, decl := range z.Specs {
					switch z.Tok {
					case token.VAR, token.CONST:
						for _, n := range decl.(*ast.ValueSpec).Names {
							st.genStoreID(fname, n)
						}
					case token.TYPE:
						ts := decl.(*ast.TypeSpec)
						name := ts.Name
						st.genStoreID(fname, name)
						switch t := ts.Type.(type) {
						case nil:
						case *ast.StructType:
							for _, f := range t.Fields.List {
								for _, n := range f.Names {
									st.genStoreID(fname, n)
								}
							}
						}
					}
				}
			case *ast.FuncDecl:
				st.genStoreID(fname, z.Name)
			}
		}
	}

}

// MarkRefs adds references to references to the imported packages in a
// file.
func (st *State) MarkRefs() error {
	if err := st.marker.SetTokenClassNoNode(util.Span, token.PACKAGE,
		st.fast.Package, st.classConf.Token); err != nil {
		return fmt.Errorf("failed to mark package keyword: %s", err)
	}
	pkgNameErr := st.marker.SetRangeClassNoNode(util.Span, st.classConf.Identifier, st.fast.Name)
	if pkgNameErr != nil {
		return fmt.Errorf("failed to mark package name %q: %s", st.fast.Name.Name, pkgNameErr)
	}
	if err := st.markDeclNames(); err != nil {
		return fmt.Errorf("failed to mark top-level decls: %s", err)
	}
	errMgr := astErrorMgr{fs: st.fs}
	for _, a := range st.fast.Decls {
		switch z := a.(type) {
		case *ast.GenDecl:
			errMgr.accumulateError(z, st.markDecl(z, true))
		case *ast.FuncDecl:
			errMgr.accumulateError(z, st.markFuncDecl(z))
		}
	}

	for _, body := range st.deferredBodies {
		errMgr.accumulateError(body, st.markStatement(body))
	}

	if errMgr.hasErr() {
		return &errMgr
	}
	return nil
}

func (st *State) markDecl(z *ast.GenDecl, toplevel bool) error {
	st.setKeywordTokenClass(z.Tok, z.TokPos)
	switch z.Tok {
	// We already took care of imports, so don't try to do anything.
	case token.IMPORT:
		return nil
	case token.VAR, token.CONST:
		return st.markVarDecl(z, z.Tok == token.CONST, !toplevel)
	case token.TYPE:
		return st.markTypeDecl(z, !toplevel)
	}
	return fmt.Errorf("unrecognized decl token: %s", z.Tok)
}

func (st *State) markNewType(i *ast.Ident) error {
	return st.markNewIdentWithClass(i, st.classConf.Type)
}

func (st *State) markNewIdentWithClass(i *ast.Ident, class string) error {
	id := genID(i.Name, i)
	a, stErr := st.marker.SetRangeClass(util.Anchor, class, i)
	if stErr != nil {
		return fmt.Errorf("failed to create anchor for identifier %q: %s", i, stErr)
	}
	a.SetAttr("name", id)
	st.objFrag[i.Obj] = "#" + id
	if st.ptypInfo != nil {
		if typeObj := st.ptypInfo.Defs[i]; typeObj != nil {
			st.typeObjFrag[typeObj] = "#" + id
			_, alt, _ := st.getTypesFrag(i, typeObj)
			a.SetAttr("title", alt)
		}
	}
	return nil
}

func (st *State) markNewIdent(i *ast.Ident) error {
	return st.markNewIdentWithClass(i, st.classConf.Identifier)
}

func (st *State) markVarDecl(a *ast.GenDecl, isConst, markNames bool) error {
	errMgr := astErrorMgr{fs: st.fs}
	var lastVals []ast.Expr
	// We're now left with variable and constant declarations, either way,
	// []spec is of type []*ValueSpec.
	for iotaNum, sp := range a.Specs {
		sp := sp.(*ast.ValueSpec)
		vals := sp.Values
		if len(vals) == 0 {
			vals = lastVals
		} else {
			lastVals = vals
		}
		for i, n := range sp.Names {
			if markNames {
				class := st.classConf.Identifier
				if isConst {
					class = st.classConf.Constant
				}
				errMgr.accumulateError(n, st.markNewIdentWithClass(n, class))
			}
			if !isConst {
				continue
			}
			// use the constant evaluation, but let some other logic override
			// it further down.
			if st.ptypInfo != nil {
				if tv, ok := st.ptypInfo.Types[vals[i]]; ok {
					st.constVals[n.Obj] = tv.Value.String()
				}
			}

			switch v := vals[i].(type) {
			case *ast.BasicLit:
				st.constVals[n.Obj] = v.Value
			case *ast.Ident:
				if v.Obj != nil && v.Obj.Type == ast.Con {
					st.constVals[n.Obj] = st.constVals[v.Obj]
				} else if v.Name == "iota" {
					st.constVals[n.Obj] = strconv.Itoa(iotaNum)
				}

			}
		}
		for _, v := range sp.Values {
			errMgr.accumulateError(v, st.markExpr(v))
		}
		errMgr.accumulateError(sp.Type, st.markExpr(sp.Type))
	}
	return errMgr.err()

}

func byteOffsetToRuneOffset(s string, offset int) int {
	runeOffset := 0
	byteOffset := 0
	reader := strings.NewReader(s)
	for _, sz, err := reader.ReadRune(); err == nil && byteOffset < offset; _, sz, err = reader.ReadRune() {
		byteOffset += sz
		runeOffset++
	}

	return runeOffset
}

func (st *State) markTypeDecl(a *ast.GenDecl, markName bool) error {
	errMgr := astErrorMgr{fs: st.fs}
	errMgr.accumulateError(a, st.marker.SetTokenClassNoNode(util.Span, a.Tok,
		a.TokPos, st.classConf.Token))
	for _, z := range a.Specs {
		ts := z.(*ast.TypeSpec)
		if markName {
			errMgr.accumulateError(ts.Name, st.markNewType(ts.Name))
		}
		errMgr.accumulateError(ts.Type, st.markExpr(ts.Type))

	}
	return errMgr.err()
}

type astErrorMgr struct {
	errs map[ast.Node]error
	fs   *token.FileSet
}

func (a *astErrorMgr) accumulateError(n ast.Node, err error) {
	if err == nil {
		return
	}

	if a.errs == nil {
		a.errs = map[ast.Node]error{n: err}
		return
	}
	a.errs[n] = err
}

func (a *astErrorMgr) Error() string {
	b := strings.Builder{}
	for n, err := range a.errs {
		fspos := a.fs.Position(n.Pos())

		fmt.Fprintf(&b, "%T at %d:%d: {%s}\n", n, fspos.Line, fspos.Column, err)
	}

	return b.String()
}

func (a *astErrorMgr) hasErr() bool {
	return len(a.errs) > 0
}

func (a *astErrorMgr) err() error {
	if a.hasErr() {
		return a
	}
	return nil
}

func (st *State) getTypeDocLink(typ types.Type) *url.URL {
	switch t := typ.(type) {
	case nil:
		return nil
	case *types.Pointer:
		return st.getTypeDocLink(t.Elem())
	case *types.Named:
		return st.getObjDocLink(t.Obj())
	case *types.Struct:
		return nil
	}
	return nil

}

func pkgName(obj types.Object) string {
	if obj == nil {
		return ""
	}
	pkg := obj.Pkg()
	if pkg == nil {
		return ""
	}
	return pkg.Path()
}

func (st *State) getObjDocLink(obj types.Object) *url.URL {
	switch v := obj.(type) {
	case *types.Var:
		// If it's a field, we need to handle this as such, since the name
		// is not top-level and the types.Var object lacks a backreference, we
		// can't generate a URL from here that makes sense.
		if v.IsField() {
			return nil
		}
	case *types.Func:
		if sig := v.Type().(*types.Signature); sig.Recv() != nil {
			// This is a method on a type.
			recvTypDocURL := st.getTypeDocLink(sig.Recv().Type())
			if recvTypDocURL != nil {
				recvTypDocURL.Fragment += "." + v.Name()
				return recvTypDocURL
			}
		}
	default:
	}
	pkgPath := pkgName(obj)
	if pkgPath == "" && obj.Pkg() == nil {
		pkgPath = "builtin"
	}
	docURL := st.d.PkgDoc(pkgPath)
	docURL.Fragment = obj.Name()
	return docURL
}

func typeIsRune(typ types.Type) bool {
	if typ == types.Typ[types.UntypedRune] {
		return true
	}

	switch t := typ.(type) {
	case *types.Named:
		if t.Obj().Pkg() != nil {
			return false
		}

		return t.Obj().Name() == "rune"
	case *types.Basic:
		if t.Kind() == types.Rune {
			return true
		}
	}

	return false
}

func runeConstStr(constVal constant.Value) string {
	valInt, valid := constant.Int64Val(constVal)
	if valid {
		str := string([]rune{rune(valInt)})
		return fmt.Sprintf("%d (%s)", valInt, str)
	}
	return constVal.String()
}

// first return: url, second: alt text, third: CSS class
func (st *State) getTypesFrag(id *ast.Ident, obj types.Object) (string, string, string) {
	typtyp := obj.Type()
	typFrag := st.typeObjFrag[obj]
	if typFrag == "" {
		typFragURL := st.getObjDocLink(obj)
		if typFragURL != nil {
			typFrag = typFragURL.String()
		}
	}
	class := st.classConf.Identifier
	switch tobj := obj.(type) {
	case *types.Const:
		valStr := tobj.Val().String()
		if typeIsRune(tobj.Type()) {
			valStr = runeConstStr(tobj.Val())
		}
		return typFrag, fmt.Sprintf("type %s; value %s", tobj.Type(), valStr),
			st.classConf.Constant
	case *types.TypeName:
		class = st.classConf.Type
	case *types.Func, *types.Builtin:
		class = st.classConf.Function
	case *types.Label:
		class = st.classConf.Special
		return typFrag, "label", class
	default:
		if tobj.Pkg() == nil {
			class = st.classConf.Builtin
		}
	}

	if v, isVar := obj.(*types.Var); isVar {
		ssaPkg := st.ssa.Package(st.ptypes)
		np := st.sa.NodePath(id)
		val, isAddr := st.ssa.VarValue(v, ssaPkg, np)
		// skip compicated expressions for now
		if !isAddr {
			switch c := val.(type) {
			case *ssa.Const:
				log.Printf("found constant value: %s", c.Value)
				return typFrag, fmt.Sprintf("type %s; value: %s", typtyp, c.Value), class
			case nil:
				log.Printf("failed to lookup ident's ssa value: %s; path: %+v", id, np)
			}
		}

	}

	return typFrag, fmt.Sprintf("type %s", typtyp.String()), class
}

func (st *State) markExprToken(e ast.Expr,
	tok token.Token, pos token.Pos) error {
	if t, ok := st.ptypInfo.Types[e]; ok {
		n, rangeErr := st.marker.SetTokenClass(
			util.Span, tok, pos, st.classConf.Token)
		if rangeErr != nil {
			return rangeErr
		}
		alt := ""
		if t.Type != nil {
			alt = fmt.Sprintf("type %s", t.Type)
		}
		if t.Value != nil {
			valStr := t.Value.String()
			if typeIsRune(t.Type) {
				valStr = runeConstStr(t.Value)
			}
			alt += fmt.Sprintf("; val %s", valStr)

		}
		if alt != "" {
			n.SetAttr("title", alt)
		}
		return nil
	}
	return st.setKeywordTokenClass(tok, pos)
}

func (st *State) markExpr(e ast.Expr) error {
	errMgr := astErrorMgr{fs: st.fs}
	ast.Inspect(e, func(n ast.Node) bool {
		switch ln := n.(type) {
		case nil:
			return false
		case *ast.Ident:
			{
				class := st.classConf.Identifier
				if ln.Name == "iota" {
					class = st.classConf.Token
				}

				typObj := st.ptypInfo.ObjectOf(ln)
				if typObj == nil {
					switch ln.Name {
					case "_":
					default:
						errMgr.accumulateError(ln, fmt.Errorf("failed to identify object for %q", ln.Name))
					}
					return false
				}
				frag, alt, class := st.getTypesFrag(ln, typObj)

				if frag == "" {
					obj := ln.Obj
					if obj == nil {
						errMgr.accumulateError(ln, st.marker.SetRangeClassNoNode(util.Anchor, st.classConf.Identifier, ln))
						return false
					}
					// fall back to ast-object info if we failed to find the object.
					if objfrag, ok := st.objFrag[obj]; ok {
						frag = objfrag
					}
					switch obj.Kind {
					case ast.Con:
						class = st.classConf.Constant
						alt = "const val = " + st.constVals[obj]
					case ast.Fun:
						class = st.classConf.Function
					case ast.Typ:
						class = st.classConf.Type
					case ast.Pkg:
						if pkgFrag, hasFrag := st.impFrag[ln.Name]; hasFrag {

							frag = pkgFrag.frag
						}
					case ast.Lbl, ast.Bad:
					}
				}
				a, identErr := st.marker.SetRangeClass(util.Anchor, class, ln)
				if identErr != nil {
					errMgr.accumulateError(ln, identErr)
					return false
				}

				if frag != "" {
					a.SetAttr("href", frag)
				}
				if alt != "" {
					a.SetAttr("title", alt)
				}
			}
		case *ast.BasicLit:
			if constClassErr := st.marker.SetRangeClassNoNode(util.Span, st.classConf.Literal, ln); constClassErr != nil {
				errMgr.accumulateError(ln, constClassErr)
				return false
			}
		case *ast.BinaryExpr:
			errMgr.accumulateError(ln, st.markExprToken(ln, ln.Op, ln.OpPos))
		case *ast.UnaryExpr:
			errMgr.accumulateError(ln, st.markExprToken(ln, ln.Op, ln.OpPos))
		case *ast.StarExpr:
			errMgr.accumulateError(ln, st.markExprToken(ln, token.MUL, ln.Star))
		case *ast.Field:
			for _, f := range ln.Names {
				errMgr.accumulateError(f, st.markNewIdent(f))
			}
			if ln.Type != nil {
				errMgr.accumulateError(ln.Type, st.markExpr(ln.Type))
			}
			return false
		case *ast.SelectorExpr:
			{
				errMgr.accumulateError(ln, st.setKeywordTokenClass(token.PERIOD, ln.X.End()))
				if st.ptypInfo != nil {
					sel, ok := st.ptypInfo.Selections[ln]
					if ok {
						typeObj := sel.Obj()
						frag, alt, class := st.getTypesFrag(nil, typeObj)
						switch sel.Kind() {
						case types.FieldVal:
							class = st.classConf.Identifier
						case types.MethodExpr, types.MethodVal:
							class = st.classConf.Function
						default:
						}
						newA, selSetErr := st.marker.SetRangeClass(util.Anchor, class, ln.Sel)
						if selSetErr != nil {
							errMgr.accumulateError(ln, selSetErr)
							return false
						}
						if frag != "" {
							newA.SetAttr("href", frag)
						} else {
							xtyp := st.ptypInfo.TypeOf(ln.X)
							xDocURL := st.getTypeDocLink(xtyp)
							if xDocURL != nil {
								xDocURL.Fragment += "." + ln.Sel.Name
								newA.SetAttr("href", xDocURL.String())
							}
						}
						if alt != "" {
							newA.SetAttr("title", alt)
						}
						errMgr.accumulateError(ln.X, st.markExpr(ln.X))
						return false
					}
				}

				if xid, ok := ln.X.(*ast.Ident); ok {
					if impFrag, hasFrag := st.impFrag[xid.Name]; hasFrag {
						newA, xSetErr := st.marker.SetRangeClass(util.Anchor, st.classConf.Import, ln.X)
						if xSetErr != nil {
							errMgr.accumulateError(ln, xSetErr)
						} else {
							newA.SetAttr("href", impFrag.frag)
						}

						obj := st.ptypInfo.ObjectOf(ln.Sel)
						_, alt, class := st.getTypesFrag(ln.Sel, obj)

						selA, selSetErr := st.marker.SetRangeClass(util.Anchor, class, ln.Sel)
						if selSetErr != nil {
							errMgr.accumulateError(ln, selSetErr)
						} else {
							doc := *impFrag.doc
							doc.Fragment = ln.Sel.Name
							selA.SetAttr("href", doc.String())
							if alt != "" {
								selA.SetAttr("title", alt)
							}
						}
						return false
					}
				}
			}
		case *ast.CompositeLit:
			{
				// let the types package figure out which type we're dealing with
				litType := st.ptypInfo.TypeOf(ln)
				if litType == nil {
					errMgr.accumulateError(ln, fmt.Errorf(
						"failed to find the type of composite literal: %v", ln))
				}
				named, isNamed := litType.(*types.Named)
				if !isNamed {
					// If it's not a named type, just let normal identifier
					// handling work, since the type definition is local.
					return true
				}
				// This is our local package, just keep iterating down, we
				// don't need to do anything fancy to link to field-names
				if named.Obj().Pkg() == st.ptypes {
					return true
				}
				// first, strip off any names on the literal type (it's an
				// identity operation for Slice, Struct and Map types).
				unNamedType := litType.Underlying()
				switch unNamedType.(type) {
				// Note: Array, Slice and Map literals don't have any
				// special scoping, so we can just iterate down normally
				// with the visitor
				case *types.Array, *types.Slice, *types.Map:
					return true
				case *types.Struct:
					// It's a struct. We need to find the struct definition and
					// associate the field names appropriately.
					baseURL := st.getObjDocLink(named.Obj())
					errMgr.accumulateError(ln, st.markExpr(ln.Type))
					for _, l := range ln.Elts {
						if kv, ok := l.(*ast.KeyValueExpr); ok {
							if kv.Key == nil {
								continue
							}
							if id, isIdent := kv.Key.(*ast.Ident); isIdent {
								idURL := *baseURL
								idURL.Fragment += "." + id.Name
								a, identErr := st.marker.SetRangeClass(util.Anchor, st.classConf.Identifier, id)
								errMgr.accumulateError(id, identErr)
								a.SetAttr("href", idURL.String())
							} else {
								errMgr.accumulateError(kv.Key, st.markExpr(kv.Key))
							}

							errMgr.accumulateError(kv.Value, st.markExpr(kv.Value))
						} else {
							errMgr.accumulateError(l, st.markExpr(l))
						}
					}
					return false
				}
			}
		case *ast.TypeAssertExpr:
			if ln.Type == nil {
				errMgr.accumulateError(ln, st.setKeywordTokenClass(token.TYPE, ln.Lparen+1))
			}
		case *ast.StructType:
			errMgr.accumulateError(ln, st.setKeywordTokenClass(token.STRUCT, ln.Struct))
		case *ast.MapType:
			errMgr.accumulateError(ln, st.setKeywordTokenClass(token.MAP, ln.Map))
			errMgr.accumulateError(ln, st.markExpr(ln.Key))
			errMgr.accumulateError(ln, st.markExpr(ln.Value))
		case *ast.InterfaceType:
			errMgr.accumulateError(ln, st.setKeywordTokenClass(token.INTERFACE, ln.Interface))
		case *ast.CallExpr:
			errMgr.accumulateError(ln, st.setKeywordTokenClass(token.LPAREN, ln.Lparen))
			errMgr.accumulateError(ln, st.markExprToken(ln, token.RPAREN, ln.Rparen))
		case *ast.IndexExpr:
			errMgr.accumulateError(ln, st.setKeywordTokenClass(token.LBRACK, ln.Lbrack))
			errMgr.accumulateError(ln, st.markExprToken(ln, token.RBRACK, ln.Rbrack))
		case *ast.ParenExpr:
			errMgr.accumulateError(ln, st.setKeywordTokenClass(token.LPAREN, ln.Lparen))
			errMgr.accumulateError(ln, st.markExprToken(ln, token.RPAREN, ln.Rparen))
		case *ast.ChanType:
			{
				chanPos := ln.Begin
				if ln.Dir == ast.RECV {
					// if the direction is RECV, then the arrow comes first, and we
					// have to adjust it by the width of the arrow.
					errMgr.accumulateError(ln, st.setKeywordTokenClass(token.ARROW, ln.Begin))
					chanPos += token.Pos(len(token.ARROW.String()))
				}
				errMgr.accumulateError(ln, st.setKeywordTokenClass(token.CHAN, chanPos))
			}
		case *ast.FuncType:
			errMgr.accumulateError(ln, st.setKeywordTokenClass(token.FUNC, ln.Func))

			if ln.Params != nil {
				for _, param := range ln.Params.List {
					for _, name := range param.Names {
						errMgr.accumulateError(name, st.markNewIdent(name))
					}
					errMgr.accumulateError(param.Type, st.markExpr(param.Type))
				}
			}
			if ln.Results == nil {
				return false
			}
			for _, ret := range ln.Results.List {
				for _, name := range ret.Names {
					errMgr.accumulateError(name, st.markNewIdent(name))
				}
				errMgr.accumulateError(ret.Type, st.markExpr(ret.Type))
			}
			return false
		case *ast.FuncLit:
			errMgr.accumulateError(ln, st.markExpr(ln.Type))
			errMgr.accumulateError(ln, st.markStatement(ln.Body))
			return false
		}
		return true
	})
	if errMgr.hasErr() {
		return &errMgr

	}
	return nil
}

func (st *State) markStatement(s ast.Stmt) error {
	errMgr := astErrorMgr{fs: st.fs}
	switch stmt := s.(type) {
	case *ast.ExprStmt:
		return st.markExpr(stmt.X)
	case *ast.AssignStmt:
		st.setKeywordTokenClass(stmt.Tok, stmt.TokPos)
		for _, r := range stmt.Rhs {
			errMgr.accumulateError(stmt, st.markExpr(r))
		}
		if stmt.Tok == token.DEFINE {
			for _, i := range stmt.Lhs {
				if id, isid := i.(*ast.Ident); isid {
					errMgr.accumulateError(stmt, st.markNewIdent(id))
				} else {
					errMgr.accumulateError(stmt, st.markExpr(i))
				}
			}
		} else {
			for _, i := range stmt.Lhs {
				errMgr.accumulateError(stmt, st.markExpr(i))
			}
		}
	case *ast.GoStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.GO, stmt.Go))
		errMgr.accumulateError(stmt, st.markExpr(stmt.Call))
	case *ast.DeferStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.DEFER, stmt.Defer))
		errMgr.accumulateError(stmt, st.markExpr(stmt.Call))
	case *ast.IfStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.IF, stmt.If))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Init))
		errMgr.accumulateError(stmt, st.markExpr(stmt.Cond))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Body))
		if stmt.Else != nil {
			// The Else token's position is not actually denoted in the AST, so
			// assume it's one space after the close brace of the superior if
			// statement (this covers else-ifs as well)
			errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.ELSE,
				stmt.Body.Rbrace+token.Pos(len(token.RBRACE.String()))+1))
			errMgr.accumulateError(stmt, st.markStatement(stmt.Else))
		}
	case *ast.BlockStmt:
		for _, s := range stmt.List {
			errMgr.accumulateError(stmt, st.markStatement(s))
		}
	case *ast.BranchStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(stmt.Tok, stmt.TokPos))
		if stmt.Label != nil {
			errMgr.accumulateError(stmt.Label, st.markExpr(stmt.Label))
		}
	case *ast.SwitchStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.SWITCH, stmt.Switch))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Init))
		errMgr.accumulateError(stmt, st.markExpr(stmt.Tag))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Body))
	case *ast.CaseClause:
		if stmt.List == nil {
			errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.DEFAULT, stmt.Case))
		} else {
			errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.CASE, stmt.Case))
			for _, e := range stmt.List {
				errMgr.accumulateError(stmt, st.markExpr(e))
			}
		}
		for _, s := range stmt.Body {
			errMgr.accumulateError(stmt, st.markStatement(s))
		}
	case *ast.CommClause:
		if stmt.Comm == nil {
			errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.DEFAULT, stmt.Case))
		} else {
			errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.CASE, stmt.Case))
			errMgr.accumulateError(stmt, st.markStatement(stmt.Comm))
		}
		for _, s := range stmt.Body {
			errMgr.accumulateError(stmt, st.markStatement(s))
		}
	case *ast.DeclStmt:
		errMgr.accumulateError(stmt, st.markDecl(stmt.Decl.(*ast.GenDecl), false))
	case *ast.ForStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.FOR, stmt.For))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Init))
		errMgr.accumulateError(stmt, st.markExpr(stmt.Cond))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Post))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Body))
	case *ast.IncDecStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(stmt.Tok, stmt.TokPos))
		errMgr.accumulateError(stmt, st.markExpr(stmt.X))
	case *ast.LabeledStmt:
		errMgr.accumulateError(stmt, st.markStatement(stmt.Stmt))
		if stmt.Label != nil && stmt.Label.Obj != nil {
			errMgr.accumulateError(stmt, st.markNewIdent(stmt.Label))
		}
	case *ast.RangeStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.FOR, stmt.For))
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(stmt.Tok, stmt.TokPos))
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.RANGE,
			stmt.TokPos+token.Pos(len(stmt.Tok.String())+1)))
		if stmt.Tok == token.DEFINE {
			if keyID, keyIsID := stmt.Key.(*ast.Ident); keyIsID {
				errMgr.accumulateError(stmt, st.markNewIdent(keyID))
			} else {
				errMgr.accumulateError(stmt, st.markExpr(stmt.Key))
			}
			if valID, valIsID := stmt.Value.(*ast.Ident); valIsID {
				errMgr.accumulateError(stmt, st.markNewIdent(valID))
			} else {
				errMgr.accumulateError(stmt, st.markExpr(stmt.Value))
			}
		}
		errMgr.accumulateError(stmt, st.markExpr(stmt.X))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Body))
	case *ast.ReturnStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.RETURN, stmt.Return))
		for _, e := range stmt.Results {
			errMgr.accumulateError(stmt, st.markExpr(e))
		}
	case *ast.SelectStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.SELECT, stmt.Select))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Body))
	case *ast.SendStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.ARROW, stmt.Arrow))
		errMgr.accumulateError(stmt, st.markExpr(stmt.Chan))
		errMgr.accumulateError(stmt, st.markExpr(stmt.Value))
	case *ast.TypeSwitchStmt:
		errMgr.accumulateError(stmt, st.setKeywordTokenClass(token.SWITCH, stmt.Switch))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Init))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Assign))
		errMgr.accumulateError(stmt, st.markStatement(stmt.Body))

	}
	if errMgr.hasErr() {
		return &errMgr
	}
	return nil
}

func (st *State) setKeywordTokenClass(tok token.Token, pos token.Pos) error {
	return st.marker.SetTokenClassNoNode(util.Span, tok, pos, st.classConf.Token)
}

func genID(name string, n ast.Node) string {
	pos := n.Pos()
	return fmt.Sprintf("%s_%04d", name, int(pos))
}

func (st *State) markFuncDecl(f *ast.FuncDecl) error {

	errMgr := astErrorMgr{fs: st.fs}
	if f.Recv != nil {
		for _, ret := range f.Recv.List {
			for _, name := range ret.Names {
				errMgr.accumulateError(name, st.markNewIdent(name))
			}
			errMgr.accumulateError(ret.Type, st.markExpr(ret.Type))
		}
	}
	errMgr.accumulateError(f.Type, st.markExpr(f.Type))

	st.deferredBodies = append(st.deferredBodies, f.Body)
	if errMgr.hasErr() {
		return &errMgr
	}
	return nil
}
