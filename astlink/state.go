package astlink

import (
	"go/ast"
	"go/token"
	"go/types"
	"net/url"

	"golang.org/x/tools/go/ssa"

	"golang.spin-2.net/htmlgoast/util/astmark"
	"golang.spin-2.net/htmlgoast/walk"
)

// CSSClassConfig configures the class-names to set on nodes of various types.
type CSSClassConfig struct {
	Identifier, Type, Token, Function, Constant, Literal, Comment, Import, Special, Builtin, PreProc string
}

// DefaultCSSClassConfig returns a default class config where class-names match
// the field-names
func DefaultCSSClassConfig() CSSClassConfig {
	return CSSClassConfig{
		Identifier: "Identifier",
		Type:       "Type",
		Token:      "Token",
		Function:   "Function",
		Constant:   "Constant",
		Literal:    "Literal",
		Comment:    "Comment",
		Import:     "Import",
		Special:    "Special",
		Builtin:    "Builtin",
		PreProc:    "PreProc",
	}
}

type impFragAndDoc struct {
	frag string
	doc  *url.URL
}

// AddPrivatePrefix adds its arguments as a URL to the list of
// package-prefix-based overrides to the serving base URL.
func (st *State) AddPrivatePrefix(prefix, docHost string, https bool) {
	st.d.AddPrivatePrefix(prefix, docHost, https)
}

// State holds the mapping of import to import name
type State struct {
	classConf CSSClassConfig

	// DocFinder for generating documentation URLs
	d DocFinder

	m map[string]*ast.ImportSpec
	// impFrag is a map from import name to URL fragment
	impFrag map[string]impFragAndDoc

	objFrag     map[*ast.Object]string
	typeObjFrag map[types.Object]string

	constVals map[*ast.Object]string

	deferredBodies []*ast.BlockStmt

	marker *astmark.Doc

	// fileset into which the file was parsed
	fs *token.FileSet
	// fast is the ast of the parsed file
	fast *ast.File

	// types.TypeInfo (only used if non-nil)
	ptypInfo *types.Info
	ptypes   *types.Package

	sa  *walk.ShadowAST
	ssa *ssa.Program
}

// NewState constructs a new State
func NewState(marker *astmark.Doc, fs *token.FileSet, fast *ast.File,
	ptypes *types.Package, ptypInfo *types.Info, ssaprog *ssa.Program, classConf CSSClassConfig) *State {
	sa := walk.ShadowAST{}
	sa.AddChildren(fast)
	return &State{
		classConf:   classConf,
		m:           make(map[string]*ast.ImportSpec, len(fast.Imports)),
		impFrag:     make(map[string]impFragAndDoc, len(fast.Imports)),
		objFrag:     make(map[*ast.Object]string, len(fast.Decls)),
		typeObjFrag: make(map[types.Object]string),
		constVals:   make(map[*ast.Object]string),
		d:           DefaultDocFinder(),
		marker:      marker,
		fs:          fs,
		fast:        fast,
		ptypes:      ptypes,
		ptypInfo:    ptypInfo,
		sa:          &sa,
		ssa:         ssaprog,
	}
}
