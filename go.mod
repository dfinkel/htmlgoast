module golang.spin-2.net/htmlgoast

go 1.12

require (
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7
	golang.org/x/tools v0.0.0-20190809145639-6d4652c779c4
)
