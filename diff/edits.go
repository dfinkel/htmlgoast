package diff

import (
	"fmt"
	"go/ast"
)

// EditKind indicates which type of edit was made to that Node
type EditKind int

// EditKind values
const (
	KindInsert EditKind = iota
	KindRemove
	// More kinds will be added later as heuristics are added.
)

func (k EditKind) String() string {
	switch k {
	case KindInsert:
		return "insert"
	case KindRemove:
		return "remove"
	}
	return "unknown"
}

// Edit describes an edit
type Edit struct {
	// Left and right nodes (for KindInsert and KindRemove, LN and RN will be
	// nil respectively)
	LN, RN ast.Node
	Kind   EditKind

	// Path from root node down to the LN and RN nodes above.
	LPath, RPath []ast.Node
}

func (e *Edit) descendsFromLeft(n ast.Node, depth int) bool {
	if len(e.LPath) < depth {
		return false
	}
	return e.LPath[depth-1] == n
}

func (e *Edit) descendsFromRight(n ast.Node, depth int) bool {
	if len(e.RPath) < depth {
		return false
	}
	return e.RPath[depth-1] == n
}

func (e Edit) String() string {
	return fmt.Sprintf("{kind: %[1]s; %[2]T %+[2]v, %[3]T %+[3]v}", e.Kind, e.LN, e.RN)
}
