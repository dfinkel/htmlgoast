package marker

import (
	"fmt"
	"go/ast"
	"log"

	"golang.spin-2.net/htmlgoast/diff"
	"golang.spin-2.net/htmlgoast/util"
	"golang.spin-2.net/htmlgoast/util/astmark"
)

// State encapsulates the state of an HTML document, two container nodes, text
// bodies, and two file ASTs.
type State struct {
	lm, rm     *astmark.Doc
	lAST, rAST *ast.File
}

// NewState generates a new diff marker State struct.
func NewState(lm, rm *astmark.Doc, lAST, rAST *ast.File) *State {
	return &State{
		lm:   lm,
		rm:   rm,
		lAST: lAST,
		rAST: rAST,
	}
}

// Mark marks ins and del nodes for insertions/deletions
func (s *State) Mark() error {
	pw := diff.NewParallelChildWalker()
	pw.AddChildren(s.lAST)
	pw.AddChildren(s.rAST)
	edits, diffErr := pw.Diff(s.lAST, s.rAST)
	if diffErr != nil {
		return fmt.Errorf("failed to traverse ASTs: %s", diffErr)
	}
	//	prunededits, pruneErr := pw.PruneEdits(edits)
	//	if pruneErr != nil {
	//		return fmt.Errorf("failed to simplify editlist: %s", pruneErr)
	//	}
	for _, edit := range edits {
		switch edit.Kind {
		case diff.KindInsert:
			if err := s.rm.SetRangeClassNoNode(util.Ins, "", edit.RN); err != nil {
				//				return fmt.Errorf("failed to mark insert node %+v: %s", edit.RN, err)
				log.Printf("failed to mark insert node %+v: %s", edit.RN, err)
			}
		case diff.KindRemove:
			if err := s.lm.SetRangeClassNoNode(util.Del, "", edit.LN); err != nil {
				//				return fmt.Errorf("failed to mark delete node %+v: %s", edit.LN, err)
				log.Printf("failed to mark delete node %+v: %s", edit.LN, err)
			}
		}
	}

	return nil
}
