package diff

import "go/ast"

type diffStackNode struct {
	lhn, rhn               ast.Node
	lhchildren, rhchildren []ast.Node
	// offsets in the parent children slice
	lidx, ridx int
}

type diffStack struct {
	p     *ParallelChildWalker
	stack []*diffStackNode
}

func (ns *diffStack) len() int {
	return len(ns.stack)
}

func (ns *diffStack) push(n *diffStackNode) {
	ns.stack = append(ns.stack, n)
}

func (ns *diffStack) pop() *diffStackNode {
	stackLen := len(ns.stack)
	if stackLen == 0 {
		return nil
	}
	n := ns.stack[stackLen-1]
	ns.stack = ns.stack[:stackLen-1]
	return n
}

func (ns *diffStack) back() *diffStackNode {
	stackLen := len(ns.stack)
	if stackLen == 0 {
		return nil
	}
	return ns.stack[stackLen-1]
}

func (ns *diffStack) childFront() (ast.Node, ast.Node) {
	bs := ns.back()
	if bs == nil {
		return nil, nil
	}
	var lchild, rchild ast.Node
	if len(bs.lhchildren) > 0 {
		lchild = bs.lhchildren[0]
	}
	if len(bs.rhchildren) > 0 {
		rchild = bs.rhchildren[0]
	}

	return lchild, rchild
}
func (ns *diffStack) shiftChild() (ast.Node, ast.Node) {
	bs := ns.back()
	if bs == nil {
		return nil, nil
	}
	var lchild, rchild ast.Node
	if len(bs.lhchildren) > 0 {
		lchild = bs.lhchildren[0]
		bs.lhchildren = bs.lhchildren[1:]
		bs.lidx++
	}
	if len(bs.rhchildren) > 0 {
		rchild = bs.rhchildren[0]
		bs.rhchildren = bs.rhchildren[1:]
		bs.ridx++
	}

	return lchild, rchild
}

func (ns *diffStack) childBack() (ast.Node, ast.Node) {
	bs := ns.back()
	if bs == nil {
		return nil, nil
	}
	var lchild, rchild ast.Node
	if l := len(bs.lhchildren); l > 0 {
		lchild = bs.lhchildren[l-1]
	}
	if l := len(bs.rhchildren); l > 0 {
		rchild = bs.rhchildren[l-1]
	}

	return lchild, rchild
}

func (ns *diffStack) popChild() (ast.Node, ast.Node) {
	bs := ns.back()
	if bs == nil {
		return nil, nil
	}
	var lchild, rchild ast.Node
	if l := len(bs.lhchildren); l > 0 {
		lchild = bs.lhchildren[l-1]
		bs.lhchildren = bs.lhchildren[:l-1]
	}
	if l := len(bs.rhchildren); l > 0 {
		rchild = bs.rhchildren[l-1]
		bs.rhchildren = bs.rhchildren[:l-1]
	}

	return lchild, rchild
}

func (ns *diffStack) lastVisitedChild() (ast.Node, ast.Node) {
	bs := ns.back()
	if bs == nil {
		return nil, nil
	}
	lchildren := ns.p.children[bs.lhn]
	rchildren := ns.p.children[bs.rhn]
	var lchild, rchild ast.Node
	if bs.lidx > 0 && bs.lidx < len(lchildren) {
		lchild = lchildren[bs.lidx-1]
	}
	if bs.ridx > 0 && bs.ridx < len(rchildren) {
		rchild = rchildren[bs.ridx-1]
	}
	return lchild, rchild
}

func (ns *diffStack) paths() ([]ast.Node, []ast.Node) {
	lout, rout := make([]ast.Node, len(ns.stack)), make([]ast.Node, len(ns.stack))
	for i, n := range ns.stack {
		lout[i] = n.lhn
		rout[i] = n.rhn
	}
	return lout, rout
}

func (ns *diffStack) editIsDescendant(edit Edit) bool {
	depth := ns.len()
	cur := ns.back()
	if edit.descendsFromLeft(cur.lhn, depth) {
		return true
	}
	return edit.descendsFromRight(cur.rhn, depth)
}

// indicates whether the passed edit is for a decendent of the last visited child
func (ns *diffStack) editIsDescendantLVC(edit Edit) bool {
	lLVC, rLVC := ns.lastVisitedChild()
	if edit.LN != nil && lLVC == nil {
		return false
	}
	if edit.RN != nil && rLVC == nil {
		return false
	}

	if !ns.editIsDescendant(edit) {
		return false
	}

	depth := ns.len()
	if edit.descendsFromLeft(lLVC, depth+1) {
		return true
	}
	return edit.descendsFromRight(rLVC, depth+1)
}

// returns a slice of indices in edits that matches the current stack
func (ns *diffStack) inEdits(edits []Edit) []int {
	depth := ns.len()
	cur := ns.back()
	out := []int{}
	for i, edit := range edits {
		if depth < len(edit.LPath) && edit.LPath[depth-1] == cur.lhn {
			out = append(out, i)
		} else if depth < len(edit.RPath) && edit.RPath[depth-1] == cur.rhn {
			out = append(out, i)
		}
	}
	return out
}
