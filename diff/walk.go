package diff

import (
	"go/ast"
	"go/token"
	"reflect"
	"sort"
)

type nodePair struct {
	left, right ast.Node
}

// ParallelChildWalker uses ast.Walk() to generate a map of nodes to their
// children, and provides a non-recursive visitor interface that lets one
// inspect parallel nodes in an AST.
type ParallelChildWalker struct {
	children       map[ast.Node][]ast.Node
	nodePairErrors map[nodePair]error
}

// NewParallelChildWalker constructs a new ParallelChildWalker
func NewParallelChildWalker() ParallelChildWalker {
	return ParallelChildWalker{
		children:       map[ast.Node][]ast.Node{},
		nodePairErrors: map[nodePair]error{},
	}
}

// AddChildren uses ast.Walk() to add children to the children map for all
// descendant nodes in the graph.
func (p *ParallelChildWalker) AddChildren(n ast.Node) {
	if p.children == nil {
		p.children = map[ast.Node][]ast.Node{}
	}
	ast.Walk(&childMapper{parent: nil, pcw: p}, n)
}

type childMapper struct {
	parent ast.Node
	pcw    *ParallelChildWalker
}

func (c *childMapper) Visit(n ast.Node) ast.Visitor {
	if n == nil || n == c.parent {
		return nil
	}
	c.pcw.children[c.parent] = append(c.pcw.children[c.parent], n)
	return &childMapper{parent: n, pcw: c.pcw}
}

type commonRun struct {
	lskip, rskip int
	length       int
}

func (c *commonRun) cost() int {
	// return the max of lskip and rskip
	if c.lskip < c.rskip {
		return c.rskip
	}
	return c.lskip
}
func (c *commonRun) weight() int {
	return c.length
}

func (p *ParallelChildWalker) findLongestCommonRun(stack *diffStack) commonRun {
	sn := stack.back()
	candidates := make([]commonRun, 0, len(sn.lhchildren)+len(sn.rhchildren))
	for i, lc := range sn.lhchildren {
		for j, rc := range sn.rhchildren {
			if err := p.nodeDeepEqualLookup(lc, rc); err == nil {
				cand := commonRun{lskip: i, rskip: j}
				z := 1
				for ; err == nil &&
					i+z < len(sn.lhchildren) && j+z < len(sn.rhchildren); z++ {
					err = p.nodeDeepEqualLookup(sn.lhchildren[i+z], sn.rhchildren[j+z])
				}
				cand.length = z
				candidates = append(candidates, cand)
			}
		}
	}
	if len(candidates) == 0 {
		return commonRun{lskip: -1, rskip: -1}
	}
	sort.Slice(candidates, func(i, j int) bool {
		ic := candidates[i].cost()
		jc := candidates[j].cost()
		if ic > jc {
			return true
		} else if ic < jc {
			return false
		}
		iw := candidates[i].weight()
		jw := candidates[j].weight()
		return iw < jw
	})
	nc := len(candidates)
	return candidates[nc-1]
}

// PruneEdits iterates over edits to find move/insertion candidates
func (p *ParallelChildWalker) PruneEdits(edits []Edit) ([]Edit, error) {
	newEdits := make([]Edit, 0, len(edits))
EDITLOOP:
	for _, e := range edits {
		if e.LN != nil {
			// find the Right-side Parent and siblings
			rparent := e.RPath[len(e.RPath)-1]
			rsiblings := p.children[rparent]
			for _, s := range rsiblings {
				if es, err := p.Diff(e.LN, s); len(es) == 0 && err == nil {
					continue EDITLOOP
				}
			}
			if len(e.RPath) > 2 {
				rgrandParent := e.RPath[len(e.RPath)-2]
				runcles := p.children[rgrandParent]
				for _, s := range runcles {
					rcousins := p.children[s]
					if rcousins == nil {
						continue
					}
					for _, cousin := range rcousins {
						if es, err := p.Diff(e.LN, cousin); len(es) == 0 && err == nil {
							continue EDITLOOP
						}
					}
				}
			}
		}
		if e.RN != nil {
			// find the Left-side Parent and siblings
			lparent := e.LPath[len(e.LPath)-2]
			rparent := e.RPath[len(e.RPath)-2]

			lsiblings := p.children[lparent]
			for _, s := range lsiblings {
				if es, err := p.Diff(e.RN, s); len(es) == 0 && err == nil {
					continue EDITLOOP
				}
			}
			if len(e.LPath) > 3 {
				lgrandParent := e.LPath[len(e.RPath)-3]
				launts := p.children[lgrandParent]
				for _, s := range launts {
					if es, err := p.Diff(rparent, s); len(es) == 0 && err == nil {
						continue EDITLOOP
					}
					lcousins := p.children[s]
					if lcousins == nil {
						continue
					}
					for _, cousin := range lcousins {
						if es, err := p.Diff(e.RN, cousin); len(es) == 0 && err == nil {
							continue EDITLOOP
						}
					}
				}
			}
		}
		newEdits = append(newEdits, e)
	}

	return newEdits, nil
}

// Diff iterates over children of left and right depth-first
func (p *ParallelChildWalker) Diff(left, right ast.Node) ([]Edit, error) {
	stack := diffStack{p: p, stack: []*diffStackNode{&diffStackNode{lhn: left, rhn: right}}}

	edits := make([]Edit, 0, 8)
	for {
		stackNode := stack.back()
		if stackNode == nil {
			return edits, nil
		}
		curLHNode := stackNode.lhn
		curRHNode := stackNode.rhn

		if curLHNode == nil && curRHNode == nil {
			stack.pop()
			continue
		}
		// verify that neither is nil.
		if curLHNode == nil || curRHNode == nil {
			lpath, rpath := stack.paths()
			if curLHNode == nil {
				edits = append(edits, Edit{
					RN:    curRHNode,
					Kind:  KindInsert,
					LPath: lpath,
					RPath: rpath,
				})
			} else {
				edits = append(edits, Edit{
					RN:    curLHNode,
					Kind:  KindRemove,
					LPath: lpath,
					RPath: rpath,
				})
			}

			stack.pop()
			continue
		}

		if err := p.nodeDeepEqualLookup(curLHNode, curRHNode); err == nil {
			// This pair of nodes, and all their children are equal, just continue onto the next sibling.
			stack.pop()
			continue
		}

		if err := nodeShallowEqual(curLHNode, curRHNode); err != nil {
			lpath, rpath := stack.paths()
			edits = append(edits, Edit{
				LN:    curLHNode,
				Kind:  KindRemove,
				LPath: lpath,
				RPath: rpath,
			}, Edit{
				RN:    curRHNode,
				Kind:  KindInsert,
				LPath: lpath,
				RPath: rpath,
			})
			// We've already marked these nodes as non-matching, there's no point
			// in iterating into its children; pop, we go!
			stack.pop()
			continue
		}

		if curLHNode != nil && stackNode.lhchildren == nil {
			stackNode.lhchildren = p.children[curLHNode]
			if stackNode.lhchildren == nil {
				stackNode.lhchildren = []ast.Node{}
			}
		}
		if curRHNode != nil && stackNode.rhchildren == nil {
			stackNode.rhchildren = p.children[curRHNode]
			if stackNode.rhchildren == nil {
				stackNode.rhchildren = []ast.Node{}
			}
		}

		// Remove the equal prefix from the child-list
		for lbackChild, rbackChild := stack.childFront(); (lbackChild != nil) &&
			(rbackChild != nil) && (p.nodeDeepEqualLookup(lbackChild, rbackChild) == nil); lbackChild, rbackChild = stack.childFront() {
			stack.shiftChild()
		}

		// We've already found at least one differing pair of nodes.
		// Taking a trick from the patience diff algorithm, let's skip to
		// the end and pop off matching pairs of children.
		for lbackChild, rbackChild := stack.childBack(); (lbackChild != nil) &&
			(rbackChild != nil) && (p.nodeDeepEqualLookup(lbackChild, rbackChild) == nil); lbackChild, rbackChild = stack.childBack() {
			stack.popChild()
		}

		if run := p.findLongestCommonRun(&stack); run.length > 0 {
			if run.lskip > 0 {
				stackNode.lhchildren = append(stackNode.lhchildren[:run.lskip],
					stackNode.lhchildren[run.lskip+run.length-1:]...)
			} else {
				stackNode.lhchildren = stackNode.lhchildren[run.length-1:]
			}
			if run.rskip > 0 {
				stackNode.rhchildren = append(stackNode.rhchildren[:run.rskip],
					stackNode.rhchildren[run.rskip+run.length-1:]...)
			} else {
				stackNode.rhchildren = stackNode.rhchildren[run.length-1:]
			}
		}

		// if both sides are empty, pop and continue.
		if (len(stackNode.lhchildren) == 0) && (len(stackNode.rhchildren) == 0) {
			stack.pop()
			continue
		}

		// if the left side is empty and the right is not, we have a bunch of insertions to add.
		if len(stackNode.lhchildren) == 0 && len(stackNode.rhchildren) != 0 {
			lpath, rpath := stack.paths()
			for _, rc := range stackNode.rhchildren {
				edits = append(edits, Edit{
					RN:    rc,
					Kind:  KindInsert,
					LPath: lpath,
					RPath: rpath,
				})
			}
			stack.pop()
			continue
		}
		if len(stackNode.rhchildren) == 0 && len(stackNode.lhchildren) != 0 {
			lpath, rpath := stack.paths()
			for _, lc := range stackNode.lhchildren {
				edits = append(edits, Edit{
					LN:    lc,
					Kind:  KindRemove,
					LPath: lpath,
					RPath: rpath,
				})
			}
			stack.pop()
			continue
		}
		lhchild, rhchild := stack.shiftChild()
		stack.push(&diffStackNode{lhn: lhchild, rhn: rhchild})
	}
}

// Runtime types special we'll be checking specifically.
var (
	reflectVals = struct {
		astNode, astChanDir, token, tokPos, str reflect.Type
	}{
		astNode:    reflect.TypeOf((*ast.Node)(nil)).Elem(),
		astChanDir: reflect.TypeOf((*ast.ChanDir)(nil)).Elem(),
		token:      reflect.TypeOf((*token.Token)(nil)).Elem(),
		tokPos:     reflect.TypeOf((*token.Pos)(nil)).Elem(),
		str:        reflect.TypeOf((*string)(nil)).Elem(),
	}
)

func (p *ParallelChildWalker) nodeDeepEqualLookup(left, right ast.Node) error {
	np := nodePair{
		left:  left,
		right: right,
	}
	if err, ok := p.nodePairErrors[np]; ok {
		return err
	}
	err := p.nodeDeepEqual(left, right)
	p.nodePairErrors[np] = err
	return err
}

func (p *ParallelChildWalker) nodeDeepEqual(left, right ast.Node) error {
	stack := diffStack{p: p, stack: []*diffStackNode{&diffStackNode{lhn: left, rhn: right}}}

	for {
		stackNode := stack.back()
		if stackNode == nil {
			return nil
		}
		curLHNode := stackNode.lhn
		curRHNode := stackNode.rhn

		np := nodePair{
			left:  curLHNode,
			right: curRHNode,
		}

		if curLHNode == nil && curRHNode == nil {
			stack.pop()
			continue
		}
		// verify that neither is nil.
		if curLHNode == nil || curRHNode == nil {
			// this should never happen
			panic("only one side is nil")
		}

		if err, ok := p.nodePairErrors[np]; ok {
			// if we've already found a diff in this subtree, abort.
			if err != nil {
				return err
			}
			// we know there are no differences in this subtree, so just pop up
			// a level and continue
			stack.pop()
			continue
		}

		if err := nodeShallowEqual(curLHNode, curRHNode); err != nil {
			return err
		}

		if curLHNode != nil && stackNode.lhchildren == nil {
			stackNode.lhchildren = p.children[curLHNode]
			if stackNode.lhchildren == nil {
				stackNode.lhchildren = []ast.Node{}
			}
		}
		if curRHNode != nil && stackNode.rhchildren == nil {
			stackNode.rhchildren = p.children[curRHNode]
			if stackNode.rhchildren == nil {
				stackNode.rhchildren = []ast.Node{}
			}
		}

		// if both sides are empty, pop and continue.
		if (len(stackNode.lhchildren) == 0) && (len(stackNode.rhchildren) == 0) {
			stack.pop()
			continue
		}

		if len(stackNode.lhchildren) != len(stackNode.rhchildren) {
			return &mismatchedChildCount{
				l:         stackNode.lhn,
				r:         stackNode.rhn,
				lchildren: len(stackNode.lhchildren),
				rchildren: len(stackNode.rhchildren),
			}
		}

		lhchild, rhchild := stack.shiftChild()
		// we've already guaranteed that there is at least one child left on
		// one side, so we can be certain we won't get a nil pointer here.
		stack.push(&diffStackNode{lhn: lhchild, rhn: rhchild})
	}
}

func nodeShallowEqual(lNode, rNode ast.Node) error {
	lType := reflect.TypeOf(lNode).Elem()
	rType := reflect.TypeOf(rNode).Elem()
	if lType != rType {
		return &mismatchedNodeTypes{
			left:  lNode,
			right: rNode,
		}
	}

	// Strip off the outer interface type when getting the type.
	lVal := reflect.ValueOf(lNode).Elem()
	rVal := reflect.ValueOf(rNode).Elem()

	nFields := lVal.NumField()
FIELDLOOP:
	for z := 0; z < nFields; z++ {
		lfVal := lVal.Field(z)
		rfVal := rVal.Field(z)

		switch lfInt := lfVal.Interface().(type) {
		case ast.Node:
			// This field will be handled by the outer comparison loop.
			continue FIELDLOOP
		case token.Token:
			{
				rTok := rfVal.Interface().(token.Token)
				if lfInt != rTok {
					return &mismatchedToken{
						lTok:  lfInt,
						rTok:  rTok,
						field: z,
					}
				}
			}
		case token.Pos:
			{
				// These positions came from different filesets, which may have
				// iterated over files in different orders, so they aren't
				// directly comparable.
				rPos := rfVal.Interface().(token.Pos)
				if (lfInt == token.NoPos) != (rPos == token.NoPos) {
					return &mismatchedPosExistence{
						lPos:  lfInt,
						rPos:  rPos,
						field: z,
					}
				}
			}
		case ast.ChanDir:
			{
				rdir := rfVal.Interface().(ast.ChanDir)
				if lfInt != rdir {
					return &mismatchedChanDirField{
						l:     lfInt,
						r:     rdir,
						field: z,
					}
				}
			}
		case string:
			{
				rStr := rfVal.String()
				if lfInt != rStr {
					return &mismatchedStringField{
						l:     lfInt,
						r:     rStr,
						field: z,
					}
				}
			}
		default:
			// all other types get skipped.
			continue FIELDLOOP
		}
	}

	return nil
}
