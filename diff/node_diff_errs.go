package diff

import (
	"fmt"
	"go/ast"
	"go/token"
	"strconv"
)

type mismatchedNodeTypes struct {
	left, right ast.Node
}

func (m *mismatchedNodeTypes) Error() string {
	return fmt.Sprintf("mismatched node types Left: %T, Right: %T on node",
		m.left, m.right)
}

type mismatchedToken struct {
	lTok, rTok token.Token
	field      int
}

func (m *mismatchedToken) Error() string {
	return fmt.Sprintf("mismatched token at field idx %d, Left: %s, Right: %s on node",
		m.field, m.lTok, m.rTok)
}

type mismatchedPosExistence struct {
	lPos, rPos token.Pos
	field      int
}

func (m *mismatchedPosExistence) Error() string {
	return fmt.Sprintf("mismatched position existence at field idx %d, Left: %d, Right: %d on node",
		m.field, m.lPos, m.rPos)
}

type mismatchedStringField struct {
	l, r  string
	field int
}

func (m *mismatchedStringField) Error() string {
	return fmt.Sprintf("mismatched string field at index %d, Left: %q, Right: %q on node",
		m.field, m.l, m.r)
}

type mismatchedChanDirField struct {
	l, r  ast.ChanDir
	field int
}

type mismatchedChildCount struct {
	l, r                 ast.Node
	lchildren, rchildren int
}

func (m *mismatchedChildCount) Error() string {
	return fmt.Sprintf("mismatched child counts: left %d; right %d",
		m.lchildren, m.rchildren)
}

func astChanDirString(dir ast.ChanDir) string {
	// if both directions are either specified or unspecified, it's
	// omnidirectional
	if (dir&ast.SEND > 0) == (dir&ast.RECV > 0) {
		return "send+recv"
	}
	// now we now it's one direction.
	switch dir {
	case ast.SEND:
		return "send"
	case ast.RECV:
		return "recv"
	default:
		return "unknown: " + strconv.Itoa(int(dir))
	}
}

func (m *mismatchedChanDirField) Error() string {
	return fmt.Sprintf("mismatched Channel Direction field at index %d, Left: %s, Right: %s on node",
		m.field, astChanDirString(m.l), astChanDirString(m.r))
}
