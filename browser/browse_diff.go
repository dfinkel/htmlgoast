package browser

import (
	"fmt"
	"go/ast"
	"go/format"
	"go/token"
	"html/template"
	"log"
	"net/http"
	"strings"

	"golang.org/x/tools/go/packages"
	"golang.org/x/tools/go/ssa"

	"golang.spin-2.net/htmlgoast/astlink"
	"golang.spin-2.net/htmlgoast/diff/marker"
	"golang.spin-2.net/htmlgoast/util"
	"golang.spin-2.net/htmlgoast/util/astmark"
)

var difftemplate = template.Must(template.New("diffsrc").Parse(`
<html>
<head>
<meta charset="utf-8" />
<style type="text/css">
<!--
#container {
	width: 100%;
	margin: 0 auto;
}
pre {
	white-space: pre-wrap;
	font-family: monospace;
	color: #c6c6c6;
	background-color: #000000;
	width: 48%;
	float: left;
}
body { font-family: monospace; color: #c6c6c6; background-color: #000000; }
ins {background-color: #001800;}
del {background-color: #180000;}
* { font-size: 1em; }
.Type { color: #bdb76b; font-weight: bold; }
.Import { color: #ffaf70; text-decoration: none; }
.Constant { color: #8080e0; }
.Literal { color: #ffafaf; }
.Identifier { color: #c6c8c6; }
.Function { color: #76ff76; }
.Token { color: #f0e68c; font-weight: bold; }
.Special { color: #ffdead; }
.Builtin { color: #ffdead; }
.Comment { color: #87ceeb; }
.PreProc { color: #d75f5f; }
-->
</style>
<title>{{.Filename}} in {{.ImportPath}}</title>
<h1>{{.Filename}} in package {{.ImportPath}}</h1>
{{if eq .LHRevision "workdir"}}Left from Working directory{{else}}
<h3>Left at revision {{.LHRevision}}</h3>
{{end}}
{{if eq .RHRevision "workdir"}}Right from Working directory{{else}}
<h3>Right at revision {{.RHRevision}}</h3>
{{end}}
</head><body>
<hr>
<div id="container">
<pre><code id="lhcodecontainer">
{{- .LHCode -}}
</code></pre>
<pre><code id="rhcodecontainer">
{{- .RHCode -}}
</code></pre>
</div>
</body>
</html>
`))

func formatAST(fs *token.FileSet, fast *ast.File) (string, error) {
	contentsbuilder := strings.Builder{}
	if fmtErr := format.Node(&contentsbuilder, fs, fast); fmtErr != nil {
		return "", fmt.Errorf("Unable to format file: %s", fmtErr)
	}
	return contentsbuilder.String(), nil
}

func (sh *SourceHandler) servePkgFileDiff(w http.ResponseWriter, r *http.Request, pkg,
	file string, lhppkg *packages.Package, lhrevision string, lhssaProg *ssa.Program, rhppkg *packages.Package, rhrevision string, rhssaProg *ssa.Program) {
	lhfast := findFile(lhppkg, file)
	if lhfast == nil {
		http.Error(w, fmt.Sprintf("Unable to find left file %q in package %q", file, pkg), http.StatusNotFound)
		return
	}
	rhfast := findFile(rhppkg, file)
	if rhfast == nil {
		http.Error(w, fmt.Sprintf("Unable to find right file %q in package %q", file, pkg), http.StatusNotFound)
		return
	}
	lhcontentsBuilder := strings.Builder{}
	if fmtErr := format.Node(&lhcontentsBuilder, lhppkg.Fset, lhfast); fmtErr != nil {
		http.Error(w, fmt.Sprintf("Unable to format left file %q in package %q: %s", file, pkg, fmtErr),
			http.StatusInternalServerError)
		return
	}
	lhfmtContents := lhcontentsBuilder.String()
	lhfmtContents, lhffmtErr := formatAST(lhppkg.Fset, lhfast)
	if lhffmtErr != nil {
		http.Error(w, fmt.Sprintf("Unable to format left file %q in package %q: %s", file, pkg, lhffmtErr),
			http.StatusInternalServerError)
		return
	}
	rhfmtContents, rhffmtErr := formatAST(rhppkg.Fset, rhfast)
	if rhffmtErr != nil {
		http.Error(w, fmt.Sprintf("Unable to format left file %q in package %q: %s", file, pkg, rhffmtErr),
			http.StatusInternalServerError)
		return
	}
	b := strings.Builder{}
	data := struct {
		ImportPath string
		Filename   string
		LHCode     string
		RHCode     string
		LHRevision string
		RHRevision string
	}{
		ImportPath: pkg,
		Filename:   file,
		LHCode:     string(lhfmtContents),
		RHCode:     string(rhfmtContents),
		LHRevision: lhrevision,
		RHRevision: rhrevision,
	}
	templErr := difftemplate.Execute(&b, &data)
	if templErr != nil {
		http.Error(w, fmt.Sprintf("unable to render template: %s", templErr),
			http.StatusInternalServerError)
		return
	}
	doc, _, parseErr := util.ParseHTML(strings.NewReader(b.String()))
	if parseErr != nil {
		http.Error(w, fmt.Sprintf("unable to parse internal bootstrap template: %s", parseErr),
			http.StatusInternalServerError)
		return
	}

	lhcontainer := doc.FindByID("lhcodecontainer")
	rhcontainer := doc.FindByID("rhcodecontainer")
	if lhcontainer == nil {
		http.Error(w, fmt.Sprintf("unable to find left code container in bootstrap template: %s", parseErr),
			http.StatusInternalServerError)
		return
	}
	if rhcontainer == nil {
		http.Error(w, fmt.Sprintf("unable to find right code container in bootstrap template: %s", parseErr),
			http.StatusInternalServerError)
		return
	}

	lhmarker := astmark.NewDoc(doc, lhcontainer, lhppkg.Fset, lhfmtContents)
	rhmarker := astmark.NewDoc(doc, rhcontainer, rhppkg.Fset, rhfmtContents)

	dmarker := marker.NewState(lhmarker, rhmarker, lhfast, rhfast)

	if err := dmarker.Mark(); err != nil {
		log.Printf("failed to diff: %s", err)
		http.Error(w, fmt.Sprintf("failed to diff: %s", err),
			http.StatusInternalServerError)
		return

	}

	lhst := astlink.NewState(lhmarker, lhppkg.Fset, lhfast, lhppkg.Types, lhppkg.TypesInfo,
		lhssaProg, sh.cssConfig)
	rhst := astlink.NewState(rhmarker, rhppkg.Fset, rhfast, rhppkg.Types, rhppkg.TypesInfo,
		rhssaProg, sh.cssConfig)

	for prefix, docSite := range sh.privateGodocs {
		lhst.AddPrivatePrefix(prefix, docSite, true)
		rhst.AddPrivatePrefix(prefix, docSite, true)
	}

	lhst.GenIDLinks(lhppkg.Syntax)
	rhst.GenIDLinks(rhppkg.Syntax)
	if err := lhst.MarkComments(); err != nil {
		log.Printf("failed to left mark comments: %s", err)
		http.Error(w, fmt.Sprintf("unable to mark left comments in file: %s", err),
			http.StatusInternalServerError)
		return
	}
	if err := rhst.MarkComments(); err != nil {
		log.Printf("failed to right mark comments: %s", err)
		http.Error(w, fmt.Sprintf("unable to mark right comments in file: %s", err),
			http.StatusInternalServerError)
		return
	}
	if err := lhst.SetImportMarks(); err != nil {
		log.Printf("failed to mark left imports: %s", err)
		http.Error(w, fmt.Sprintf("unable to mark left imports in file: %s", err),
			http.StatusInternalServerError)
		return
	}
	if err := rhst.SetImportMarks(); err != nil {
		log.Printf("failed to mark right imports: %s", err)
		http.Error(w, fmt.Sprintf("unable to mark right imports in file: %s", err),
			http.StatusInternalServerError)
		return
	}
	if err := lhst.MarkRefs(); err != nil {
		// 	http.Error(w, fmt.Sprintf("unable to mark references in file: %s", err),
		// 		http.StatusInternalServerError)
		log.Printf("failed to mark left references: %s", err)

	}
	if err := rhst.MarkRefs(); err != nil {
		// 	http.Error(w, fmt.Sprintf("unable to mark references in file: %s", err),
		// 		http.StatusInternalServerError)
		log.Printf("failed to mark right references: %s", err)

	}
	w.Header().Add("Content-Type", "text/html")
	if err := doc.Render(w); err != nil {
		log.Printf("failed to render doc: %s", err)
	}
}
