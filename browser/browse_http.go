package browser

import (
	"context"
	"fmt"
	"go/ast"
	"go/format"
	"go/parser"
	"go/token"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/tools/go/packages"
	"golang.org/x/tools/go/ssa"
	"golang.org/x/tools/go/ssa/ssautil"
	"golang.spin-2.net/htmlgoast/astlink"
	"golang.spin-2.net/htmlgoast/util"
	"golang.spin-2.net/htmlgoast/util/astmark"
	"golang.spin-2.net/htmlgoast/vcs"
)

// SourceHandler serves go source files from a directory
type SourceHandler struct {
	// packages from the working directory's revision
	workdir       revPkgs
	cssConfig     astlink.CSSClassConfig
	privateGodocs map[string]string
	vcs           vcs.Interface
	revs          map[string]revPkgs
	pattern       string
}
type revPkgs struct {
	pkgs    map[string]*packages.Package
	ssaProg *ssa.Program
}

func newrevPkgs(ctx context.Context, dir, pattern string) (*revPkgs, error) {
	pkgConf := packages.Config{
		Mode: packages.NeedCompiledGoFiles | packages.NeedFiles |
			packages.NeedDeps | packages.NeedImports | packages.NeedTypes | packages.NeedTypesInfo |
			packages.NeedSyntax,
		Context: ctx,
		// Minimally wrap parser.ParseFile with a call so we can slip in a
		// preceding call to formatter.Source()
		ParseFile: func(fset *token.FileSet, filename string, src []byte) (*ast.File, error) {
			formattedSrc, fmtErr := format.Source(src)
			if fmtErr != nil {
				return nil, fmt.Errorf("failed to format source file %q: %s", filename, fmtErr)
			}
			return parser.ParseFile(fset, filename, formattedSrc, parser.ParseComments)
		},
		Dir:        dir,
		Env:        nil,
		BuildFlags: nil,
		Tests:      true,
	}
	pkgs, err := packages.Load(&pkgConf, pattern, "builtin")
	if err != nil {
		return nil, fmt.Errorf("failed to load packages with pattern %q: %s", pattern, err)
	}
	if len(pkgs) == 0 {
		return nil, fmt.Errorf("pattern %q matched zero packages", pattern)
	}
	ssaprog, _ := ssautil.Packages(pkgs, ssa.NaiveForm|ssa.GlobalDebug)
	ssaprog.Build()
	pkgmap := make(map[string]*packages.Package)
	for _, pkg := range pkgs {
		log.Printf("adding package %q (name %q) package %q; types path %q",
			pkg.PkgPath, pkg.ID, pkg.Name, pkg.Types.Path())
		pkgmap[pkg.Types.Path()] = pkg
		for _, dep := range pkg.Imports {
			pkgmap[dep.Types.Path()] = dep
		}
	}
	return &revPkgs{pkgs: pkgmap, ssaProg: ssaprog}, nil
}

// NewSourceHandler creates a new SourceHandler for the specified import path.
// ("." should be valid)
func NewSourceHandler(ctx context.Context, pattern string,
	cssConfig astlink.CSSClassConfig, privateGodocs map[string]string) (*SourceHandler, error) {
	workdir, wdirErr := os.Getwd()
	if wdirErr != nil {
		return nil, fmt.Errorf("failed to get CWD: %s", wdirErr)
	}
	v, vcsErr := vcs.Find(workdir)
	if vcsErr != nil {
		return nil, fmt.Errorf("failed to initialize VCS support for working directory: %s", vcsErr)
	}
	workDirPkgs, wdirErr := newrevPkgs(ctx, "", pattern)
	if wdirErr != nil {
		return nil, fmt.Errorf("failed to initialize/parse working directory AST: %s", wdirErr)
	}
	return &SourceHandler{workdir: *workDirPkgs, cssConfig: cssConfig,
		privateGodocs: privateGodocs, vcs: v, pattern: pattern,
		revs: map[string]revPkgs{}}, nil
}

func (sh *SourceHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	// If there's no filename, serve an index.
	dir, file := filepath.Split(path)
	if len(dir) > 0 && dir[0] == '/' {
		dir = dir[1:]
	}
	if len(dir) > 0 && dir[len(dir)-1] == '/' {
		dir = dir[:len(dir)-1]
	}
	if file == "" {
		sh.ServeIndex(w, r, dir)
		return
	}
	sh.ServeFile(w, r, dir, file)
}

var mainIndextemplate = template.Must(template.New("index").Parse(`
<html>
<head>
  <meta charset="utf-8" />
  <style type="text/css">
  <!--
  pre { white-space: pre-wrap; font-family: monospace; color: #c6c6c6; background-color: #000000; }
  body { font-family: monospace; color: #c6c6c6; background-color: #000000; }
  a { color: #87ceeb; }
  * { font-size: 1em; }
  -->
  </style>
  <title>Index of packages</title>
  <h1>Index of packages</h1>
</head>
<body>
<hr>
<ul>
{{ range $idx, $pkg := .Packages }}<li> <a href="/{{$idx}}/">{{$idx}}</a>{{end}} </li>
</ul>
</body>
</html>
`))

var indextemplate = template.Must(template.New("index").Parse(`
<html>
<head>
<meta charset="utf-8" />
<style type="text/css">
<!--
pre { white-space: pre-wrap; font-family: monospace; color: #c6c6c6; background-color: #000000; }
body { font-family: monospace; color: #c6c6c6; background-color: #000000; }
a { color: #87ceeb; }
* { font-size: 1em; }
-->
</style>
<title>Index for package {{.ImportPath}}</title>
<h1>Index for package {{.ImportPath}}</h1></head>
<body>
<hr>
<ul>
{{$imp := .ImportPath}}
{{ range .Files }}<li> <a href="/{{$imp}}/{{.}}">{{.}}</a>{{end}} </li>
</ul>
</body>
</html>
`))

func (r *revPkgs) findPkg(pkg string) *packages.Package {
	if ppkg, ok := r.pkgs[pkg]; ok {
		return ppkg
	}

	return nil
}

// ServeIndex serves an index of go files for the requested package.
func (sh *SourceHandler) ServeIndex(w http.ResponseWriter, r *http.Request, pkg string) {
	if pkg == "" {
		data := struct {
			Packages map[string]*packages.Package
		}{
			Packages: sh.workdir.pkgs,
		}
		w.Header().Add("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)
		if execErr := mainIndextemplate.Execute(w, &data); execErr != nil {
			log.Printf("failed to render template: %s", execErr)
		}
		return
	}
	ppkg := sh.workdir.findPkg(pkg)
	if ppkg == nil {
		http.Error(w, fmt.Sprintf("Unable to find package %q", pkg), http.StatusNotFound)
		return
	}
	files := ppkg.GoFiles

	// Strip off the absolute paths:
	realFiles := make([]string, 0, len(files))
	for _, f := range files {
		realFiles = append(realFiles, filepath.Base(f))
	}
	data := struct {
		ImportPath string
		Files      []string
	}{
		ImportPath: pkg,
		Files:      realFiles,
	}
	w.Header().Add("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	if execErr := indextemplate.Execute(w, &data); execErr != nil {
		log.Printf("failed to render template: %s", execErr)
	}
}

func findFile(pkg *packages.Package, file string) *ast.File {
	fset := pkg.Fset
	for _, fast := range pkg.Syntax {
		if filepath.Base(fset.Position(fast.Pos()).Filename) == file {
			return fast
		}
	}
	return nil
}

var srctemplate = template.Must(template.New("filesrc").Parse(`
<html>
<head>
<meta charset="utf-8" />
<style type="text/css">
<!--
pre { white-space: pre-wrap; font-family: monospace; color: #c6c6c6; background-color: #000000; }
body { font-family: monospace; color: #c6c6c6; background-color: #000000; }
* { font-size: 1em; }
.Type { color: #bdb76b; font-weight: bold; }
.Import { color: #ffaf70; text-decoration: none; }
.Constant { color: #8080e0; }
.Literal { color: #ffafaf; }
.Identifier { color: #c6c8c6; }
.Function { color: #76ff76; }
.Token { color: #f0e68c; font-weight: bold; }
.Special { color: #ffdead; }
.Builtin { color: #ffdead; }
.Comment { color: #87ceeb; }
.PreProc { color: #d75f5f; }
-->
</style>
<title>{{.Filename}} in {{.ImportPath}}</title>
<h1>{{.Filename}} in package {{.ImportPath}}</h1>
{{if eq .Revision "workdir"}}From Working directory{{else}}
<h3>At revision {{.Revision}}</h3>
{{end}}
</head><body>
<hr>
<pre><code id="codecontainer">
{{- .Code -}}
</code></pre>
</body>
</html>
`))

// ServeFile generates a simple syntax-highlighted HTML document for a go file
// (if found)
func (sh *SourceHandler) ServeFile(w http.ResponseWriter, r *http.Request, pkg, file string) {
	if rev := sh.checkRevision(r); rev != "" {
		sh.serveRevision(w, r, pkg, file, rev)
		return
	}
	ppkg := sh.workdir.findPkg(pkg)
	if ppkg == nil {
		http.Error(w, fmt.Sprintf("Unable to find package %q", pkg), http.StatusNotFound)
		return
	}
	sh.servePkgFile(w, r, pkg, file, ppkg, sh.workdir.ssaProg, "workdir")
}

func (sh *SourceHandler) servePkgFile(w http.ResponseWriter, r *http.Request, pkg, file string,
	ppkg *packages.Package, ssaprog *ssa.Program, revision string) {
	fast := findFile(ppkg, file)
	if fast == nil {
		http.Error(w, fmt.Sprintf("Unable to find file %q in package %q", file, pkg), http.StatusNotFound)
		return
	}
	contentsBuilder := strings.Builder{}
	if fmtErr := format.Node(&contentsBuilder, ppkg.Fset, fast); fmtErr != nil {
		http.Error(w, fmt.Sprintf("Unable to format file %q in package %q: %s", file, pkg, fmtErr),
			http.StatusInternalServerError)
		return
	}
	fmtContents := contentsBuilder.String()
	b := strings.Builder{}
	data := struct {
		ImportPath string
		Filename   string
		Code       string
		Revision   string
	}{
		ImportPath: pkg,
		Filename:   file,
		Code:       string(fmtContents),
		Revision:   revision,
	}
	templErr := srctemplate.Execute(&b, &data)
	if templErr != nil {
		http.Error(w, fmt.Sprintf("unable to render template: %s", templErr),
			http.StatusInternalServerError)
		return
	}
	doc, _, parseErr := util.ParseHTML(strings.NewReader(b.String()))
	if parseErr != nil {
		http.Error(w, fmt.Sprintf("unable to parse internal bootstrap template: %s", parseErr),
			http.StatusInternalServerError)
		return
	}

	container := doc.FindByID("codecontainer")
	if container == nil {
		http.Error(w, fmt.Sprintf("unable to find code container in bootstrap template: %s", parseErr),
			http.StatusInternalServerError)
		return
	}

	marker := astmark.NewDoc(doc, container, ppkg.Fset, string(fmtContents))

	st := astlink.NewState(marker, ppkg.Fset, fast, ppkg.Types, ppkg.TypesInfo,
		ssaprog, sh.cssConfig)

	for prefix, docSite := range sh.privateGodocs {
		st.AddPrivatePrefix(prefix, docSite, true)
	}

	st.GenIDLinks(ppkg.Syntax)
	if err := st.MarkComments(); err != nil {
		log.Printf("failed to mark comments: %s", err)
		http.Error(w, fmt.Sprintf("unable to mark comments in file: %s", err),
			http.StatusInternalServerError)
		return
	}
	if err := st.SetImportMarks(); err != nil {
		log.Printf("failed to mark imports: %s", err)
		http.Error(w, fmt.Sprintf("unable to mark imports in file: %s", err),
			http.StatusInternalServerError)
		return
	}
	if err := st.MarkRefs(); err != nil {
		// 	http.Error(w, fmt.Sprintf("unable to mark references in file: %s", err),
		// 		http.StatusInternalServerError)
		log.Printf("failed to mark references: %s", err)

	}
	w.Header().Add("Content-Type", "text/html")
	if err := doc.Render(w); err != nil {
		log.Printf("failed to render doc: %s", err)
	}
}
