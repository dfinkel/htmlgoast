package browser

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
)

// RevQueryKey is a query parameter (key) indicating which revision to display
// of a file. (instead of the one from the working directory at startup.)
const RevQueryKey = "revision"

// DiffRevQueryKey is a query parameter (key) indicating which revision to diff
// a file against (this will be the left-hand pane, while RevQueryKey will be
// the right hand).
const DiffRevQueryKey = "diffrevision"

func (sh *SourceHandler) checkRevision(r *http.Request) string {
	query := r.URL.Query()
	return query.Get(RevQueryKey)
}

func (sh *SourceHandler) checkDiffRevision(r *http.Request) string {
	query := r.URL.Query()
	return query.Get(DiffRevQueryKey)
}

func (sh *SourceHandler) parseRevision(ctx context.Context, pkg, revision string) (*revPkgs, error) {
	rev, resolvErr := sh.vcs.ResolveToCommit(ctx, revision)
	if resolvErr != nil {
		return nil, fmt.Errorf("unable to canonicalize revision %q: %s", rev, resolvErr)
	}
	if rpkg, ok := sh.revs[rev]; ok {
		return &rpkg, nil
	}

	dir, tmpErr := ioutil.TempDir("", "goastbrowse")
	if tmpErr != nil {
		return nil, fmt.Errorf("failed to create temporary directory: %s", tmpErr)
	}
	cloneDir := filepath.Join(dir, "src")

	defer os.RemoveAll(dir)

	cloneErr := sh.vcs.CloneToDir(ctx, cloneDir, revision)
	if cloneErr != nil {
		return nil, fmt.Errorf("failed to clone repository into a temporary directory: %s", cloneErr)
	}

	rpkgs, err := newrevPkgs(ctx, cloneDir, sh.pattern)
	if err != nil {
		return nil, fmt.Errorf("failed to parse at revision with error: %s", err)
	}

	sh.revs[rev] = *rpkgs

	return rpkgs, nil
}

func (sh *SourceHandler) serveRevision(w http.ResponseWriter, r *http.Request, pkg, file, revision string) {
	rpkg, ok := sh.revs[revision]
	if !ok {
		parsedRPkg, parseErr := sh.parseRevision(r.Context(), pkg, revision)
		if parseErr != nil {
			http.Error(w, parseErr.Error(),
				http.StatusInternalServerError)
			return
		}
		rpkg = *parsedRPkg
	}
	ppkg := rpkg.findPkg(pkg)
	if ppkg == nil {
		http.Error(w, fmt.Sprintf("unable to find package %s", pkg),
			http.StatusNotFound)
		return
	}

	if lhrev := sh.checkDiffRevision(r); lhrev != "" {
		if lhpkg, ok := sh.revs[lhrev]; ok {
			lhppkg := lhpkg.findPkg(pkg)
			if lhppkg == nil {
				http.Error(w, fmt.Sprintf("unable to find package %s at diff revision %s", pkg, lhrev),
					http.StatusNotFound)
				return
			}
			sh.servePkgFileDiff(w, r, pkg, file, lhppkg, lhrev, lhpkg.ssaProg,
				ppkg, revision, rpkg.ssaProg)
			return
		}
		parsedLPkg, parseErr := sh.parseRevision(r.Context(), pkg, lhrev)
		if parseErr != nil {
			http.Error(w, "unable to parse package at diff revision: "+parseErr.Error(),
				http.StatusInternalServerError)
			return
		}
		lhppkg := parsedLPkg.findPkg(pkg)
		if lhppkg == nil {
			http.Error(w, fmt.Sprintf("unable to find package %s at diff revision %s", pkg, lhrev),
				http.StatusNotFound)
			return
		}
		sh.servePkgFileDiff(w, r, pkg, file, lhppkg, lhrev, parsedLPkg.ssaProg,
			ppkg, revision, rpkg.ssaProg)
		return
	}

	sh.servePkgFile(w, r, pkg, file, ppkg, rpkg.ssaProg, revision)
}
