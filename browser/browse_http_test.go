package browser

import (
	"context"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"

	"golang.spin-2.net/htmlgoast/astlink"
)

func TestParseAndRenderSelf(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	const pkg = "golang.spin-2.net/htmlgoast"

	handler, err := NewSourceHandler(ctx, pkg+"/...", astlink.DefaultCSSClassConfig(), nil)

	if err != nil {
		t.Fatalf("failed to construct handler: %s", err)
	}

	rec := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, filepath.Join("/", pkg, "browser/browse_http_test.go"), nil)

	handler.ServeHTTP(rec, req)
	resp := rec.Result()

	if resp.StatusCode != http.StatusOK {
		t.Errorf("unexpected status: %d", resp.StatusCode)
	}
}
