package walk

import "go/ast"

type parentDepth struct {
	n     ast.Node
	depth int
}

// ShadowAST tracks the children and parents of individual nodes
type ShadowAST struct {
	children map[ast.Node][]ast.Node
	parents  map[ast.Node]parentDepth
}

// AddChildren uses ast.Walk() to add children to the children map for all
// descendant nodes in the graph.
func (s *ShadowAST) AddChildren(n ast.Node) {
	if s.children == nil {
		s.children = map[ast.Node][]ast.Node{}
	}
	if s.parents == nil {
		s.parents = map[ast.Node]parentDepth{}
	}
	s.parents[n] = parentDepth{n: nil, depth: 0}
	ast.Walk(&childMapper{parent: nil, sast: s, depth: 0}, n)
}

type childMapper struct {
	parent ast.Node
	sast   *ShadowAST
	depth  int
}

func (c *childMapper) Visit(n ast.Node) ast.Visitor {
	if n == nil || n == c.parent {
		return nil
	}
	c.sast.children[c.parent] = append(c.sast.children[c.parent], n)
	c.sast.parents[n] = parentDepth{n: c.parent, depth: c.depth}
	return &childMapper{parent: n, sast: c.sast, depth: c.depth + 1}
}

// Parent returns the parent node of its argument
func (s *ShadowAST) Parent(n ast.Node) ast.Node {
	p, ok := s.parents[n]
	if !ok {
		return nil
	}
	return p.n
}

// Children returns all children of its argument as iterated by ast.Visitor
func (s *ShadowAST) Children(n ast.Node) []ast.Node {
	return s.children[n]
}

// NodePath returns the path from n to the root of the longest AST that was
// passed to AddChildren including that node.
// Returns the nodes with the leaf-nodes first (to match the ssa package's
// expectations).
func (s *ShadowAST) NodePath(n ast.Node) []ast.Node {
	p, ok := s.parents[n]
	if !ok {
		return nil
	}
	path := make([]ast.Node, 1, p.depth)
	path[0] = n
	for ok && p.n != nil {
		path = append(path, p.n)
		p, ok = s.parents[p.n]
	}

	return path
}

func (s *ShadowAST) foobar() int {
	q := 200
	return q

}
