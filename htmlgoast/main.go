package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	// include the excellent http/pprof handlers
	_ "net/http/pprof"

	"golang.spin-2.net/htmlgoast/astlink"
	"golang.spin-2.net/htmlgoast/browser"
)

var (
	listenPort    = flag.Int("port", 8080, "HTTP port to listen on")
	listenAddr    = flag.String("listen_addr", "", "listen address (may be the empty string for all interfaces)")
	privGodocHost = flag.String("private_godoc_hosts", "",
		"comma-separated key:value pairs of package prefixes to serve docs with particular host")
)

func main() {
	flag.Parse()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	if flag.NArg() != 1 {
		fmt.Fprintf(os.Stderr, "supplied %d; expected 1 argument", flag.NArg())

		os.Exit(32)
	}
	pkg := flag.Arg(0)

	privatePrefixes := strings.Split(*privGodocHost, ",")
	if len(*privGodocHost) == 0 {
		privatePrefixes = []string{}
	}
	privDocs := make(map[string]string, len(privatePrefixes))

	for _, kv := range privatePrefixes {
		parts := strings.SplitN(kv, ":", 2)
		if len(parts) < 2 {
			continue
		}
		privDocs[parts[0]] = parts[1]
	}

	handler, handleErr := browser.NewSourceHandler(ctx, pkg, astlink.DefaultCSSClassConfig(), privDocs)
	if handleErr != nil {
		fmt.Fprintf(os.Stderr, "failed to initialize handler: %s", handleErr)

		os.Exit(2)
	}
	http.Handle("/", handler)
	fmt.Printf("initialized state for package %q\n", pkg)
	httpErr := http.ListenAndServe(*listenAddr+":"+strconv.Itoa(*listenPort), nil)
	if httpErr != nil {
		log.Fatalf("http server shutdown: %s", httpErr)
	}

}
